//
//  AccountSettingsVC.swift
//  LockBox
//
//  Created by max kryuchkov on 17.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AccountSettingsVC: BasicVC {
    
    // MARK: - Constants
    
    static let API_LOGOUT_URL = ResourceUtil.getApiUrl(key: "user_logout_url")
    static let API_CHANGE_USER_PASS_URL = ResourceUtil.getApiUrl(key: "change_user_pass_url")
    static let API_REMOVE_USER_URL = ResourceUtil.getApiUrl(key: "remove_user_url")
    
    // MARK: - Outlets
    
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var currentPasswordField: ValidatedTextField!
    @IBOutlet weak var newPasswordField: ValidatedTextField!
    @IBOutlet weak var confirmPasswordField: ValidatedTextField!
    
    // MARK: - Properties
    
    var userProfile: NSObject?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupFields()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        currentPasswordField.subviews.first?.fillSuperview()
        newPasswordField.subviews.first?.fillSuperview()
        confirmPasswordField.subviews.first?.fillSuperview()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let userEmail = UserDefaults.standard.object(forKey: "email") as? String {
            emailLabel.text = userEmail
        }
    }
    
    // MARK: - Setup
    
    private func setupFields() {
        currentPasswordField.placeholder = "Current Password"
        currentPasswordField.secureState = .secure(isTextVisible: false)
        currentPasswordField.delegate = self
        
        newPasswordField.placeholder = "New Password"
        newPasswordField.secureState = .secure(isTextVisible: false)
        newPasswordField.delegate = self
        
        confirmPasswordField.placeholder = "Confirm New Password"
        confirmPasswordField.secureState = .secure(isTextVisible: false)
        confirmPasswordField.delegate = self
    }
    
    // MARK: - Actions
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeEmailTapped(_ sender: UIButton) {
        navigationController?.pushViewController(ChangeEmailVC(), animated: true)
    }
    
    @IBAction func deleteAccountTapped(_ sender: UIButton) {
        let alert = AlertController(
            title: "Account Removal",
            message: "Please confirm the removal of your account. All related data including registered devices and their event history will be completely removed as well.",
            style: .default)
        let cancelAction = AlertAction(title: "Cancel", action: {})
        let okAction = AlertAction(title: "OK", action: {
            self.deleteAccountRequest()
        })
        
        alert.add(action: cancelAction)
        alert.add(action: okAction)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func logoutTapped(_ sender: Any) {
        
        guard let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        guard let token = AppDelegate.deviceToken,
            let vendorId = AppDelegate.currentDeviceUid else {
                self.showPopUp(
                    title: "Log out failed",
                    message: "Cannot obtain device token for push notifications or vendor id")
                return
        }
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": vendorId,
            "X-DEVICE-PUSH-TOKEN": token
        ]
        
        Request.go(vc: self,
                   api: AccountSettingsVC.API_LOGOUT_URL,
                   method: .post,
                   headers: headers,
                   paramData: nil,
                   requestTitle: "Logout",
                   onSuccessClojure: { [weak self] (json: JSON) in
                    self?.goToSplashScreen(isEmergency: false)
        })
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        guard currentPasswordField.state == .filled else { return }
        guard newPasswordField.state == .filled else { return }
        guard confirmPasswordField.state == .filled else { return }
        
        changeUserPassRequest()
    }
    
    private func deleteAccountRequest() {
        guard let userId = UserDefaults.standard.string(forKey: "user_id"),
            let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        
        let apiUrl = AccountSettingsVC.API_REMOVE_USER_URL
            .replacingOccurrences(of: "{{user_id}}", with: userId)
        
        Request.go(vc: self,
                   api: apiUrl,
                   method: .delete,
                   headers: headers,
                   paramData: nil,
                   requestTitle: "Account settings",
                   onSuccessClojure: { [weak self] (json: JSON) in
                    self?.goToSplashScreen(isEmergency: false)
        })
    }
    
    private func changeUserPassRequest() {
        guard let userId = UserDefaults.standard.string(forKey: "user_id"),
            let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        let passData = [
            "current_password": currentPasswordField.text,
            "new_password": newPasswordField.text,
            "new_password_confirmation": confirmPasswordField.text
        ]
        let apiUrl = AccountSettingsVC.API_CHANGE_USER_PASS_URL
            .replacingOccurrences(of: "{{user_id}}", with: userId)
        
        Alamofire.request(apiUrl,
                          method: .put,
                          parameters: passData,
                          encoding: JSONEncoding.default,
                          headers: headers)
            .responseJSON { [weak self] response in
                let requestTitle = "Account Settings"
                guard let strongSelf = self else {
                    print("No self reference is available")
                    return
                }
                // if unauthorized
                if response.response?.statusCode == 401 {
                    strongSelf.goToSplashScreen()
                    return
                }
                switch response.result {
                case .success:
                    guard let d = response.data, let regResponseObj = try? JSON(data: d) else {
                        strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                        return
                    }
                    
                    if response.response?.statusCode == 422 {
                        if var msg = regResponseObj["message"].string {
                            msg.removingRegexMatches(pattern: "^\"[a-zA-Z0-9_\\s\\-]*\"")
                            strongSelf.showPopUp(title: requestTitle, message: msg)
                            return
                        }
                    }
                    if let status = regResponseObj["status"].string, let msg = regResponseObj["message"].string {
                        strongSelf.showPopUp(title: "Error", message: msg)
                        
                        print("Request finished with status: \(status) and message: \(msg)")
                        return
                    }
                    if regResponseObj["success"].boolValue {
                        
                        let alert = AlertController(
                            title: "Success",
                            message: "Your password has been changed successfully.",
                            style: .default)
                        let okAction = AlertAction(title: "OK", action: {
                            strongSelf.clearFields()
                        })
                        alert.add(action: okAction)
                        strongSelf.present(alert, animated: true, completion: nil)
                        
                    } else {
                        if let errMessage = regResponseObj["error"].string {
                            strongSelf.showPopUp(title: requestTitle, message: errMessage)
                        } else {
                            strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                        }
                    }
                case .failure(let error):
                    if error._code == NSURLErrorTimedOut {
                        strongSelf.showPopUp(title: requestTitle, message: "The Parcel Vault seems to be offline. Please check its Internet connection and try later.")
                    } else {
                        strongSelf.showPopUp(title: requestTitle, message: "Network error")
                    }
                }
        }
    }
    
    private func sliceFieldName(field: String) -> String{
        return ""
    }
    
    private func clearFields() {
        currentPasswordField.text = ""
        newPasswordField.text = ""
        confirmPasswordField.text = ""
    }
}

// MARK: - Validated Field Delegate

extension AccountSettingsVC: ValidatedTextFieldDelegate {
    
    func validatedStateFor(textField: ValidatedTextField) -> Validator.ValidationState {
        
        if textField == newPasswordField {
            if newPasswordField.text == confirmPasswordField.text {
                confirmPasswordField.state = .filled
            } else {
                confirmPasswordField.state = .error(message: "The entered passwords do not match.")
            }
            return Validator.shared.validate(password: textField.text)
        }
        
        if textField == confirmPasswordField {
            
            switch Validator.shared.validate(password: newPasswordField.text) {
                case .invalid(let r): newPasswordField.state = .error(message: r)
                default: newPasswordField.state = .filled
            }
            
            if confirmPasswordField.text == newPasswordField.text {
                return .valid
            } else {
                return .invalid(reason: "The entered passwords do not match.")
            }
        }
        
        return .valid
    }
}

extension String {
    mutating func removingRegexMatches(pattern: String, replaceWith: String = "") {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: [.caseInsensitive, .anchorsMatchLines])
            let range = NSMakeRange(0, self.count)
            self = regex.stringByReplacingMatches(in: self, options: [], range: range, withTemplate: replaceWith)
        } catch {
            return
        }
    }
}
