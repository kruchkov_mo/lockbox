//
//  AccountSettingsVC.swift
//  LockBox
//
//  Created by max kruchkov on 17.09.2018.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ChangeEmailVC: BasicVC {
    
    // MARK: - Constants
    
    static let API_CHANGE_EMAIL_URL = ResourceUtil.getApiUrl(key: "change_email_url")
    
    // MARK: - Outlets
    
    @IBOutlet weak var currentEmail: UILabel!
    @IBOutlet weak var newEmail: ValidatedTextField!
    
    // MARK: - Properties
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupFields()
        
        if let userEmail = UserDefaults.standard.object(forKey: "email") as? String {
            currentEmail.text = userEmail
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        newEmail.subviews.first?.fillSuperview()
    }
    
    // MARK: - Setup
    
    private func setupFields() {
        newEmail.placeholder = "New Email"
        newEmail.secureState = .notSecure
        newEmail.delegate = self
    }
    
    // MARK: - Actions
    
    @IBAction func backTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitTapped(_ sender: UIButton) {
        guard newEmail.state == .filled else { return }
        
        changeUserEmailRequest()
    }
    
    private func changeUserEmailRequest() {
        guard let userId = UserDefaults.standard.string(forKey: "user_id"),
            let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        let passData = [
            "email": newEmail.text,
        ]
        let apiUrl = ChangeEmailVC.API_CHANGE_EMAIL_URL
            .replacingOccurrences(of: "{{user_id}}", with: userId)
        
        Request.go(vc: self,
                   api: apiUrl,
                   method: .put,
                   headers: headers,
                   paramData: passData,
                   requestTitle: "Change email",
                   onSuccessClojure: {[weak self] (json: JSON) in
                    
                    UserDefaults.standard.set(json["user"]["email"].stringValue, forKey: "email")
                    
                    guard let strongSelf = self else {
                        return
                    }
                    
                    let alert = AlertController(title: "Success", message: json["message"].stringValue, style: .default)
                    let okAction = AlertAction(title: "OK", action: {
                        strongSelf.clearFields()
                        strongSelf.navigationController?.popViewController(animated: true)
                    })
                    alert.add(action: okAction)
                    strongSelf.present(alert, animated: true, completion: nil)
                    
        })
    }
    
    private func clearFields() {
        newEmail.text = ""
    }
}

// MARK: - Validated Field Delegate

extension ChangeEmailVC: ValidatedTextFieldDelegate {
    
    func validatedStateFor(textField: ValidatedTextField) -> Validator.ValidationState {
        
        if textField == newEmail {
            return Validator.shared.validate(email: textField.text)
        }
        
        return .valid
    }
}
