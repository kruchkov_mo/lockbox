//
//  LockboxDetailsVC.swift
//  LockBox
//
//  Created by max kryuchkov on 17.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import UserNotifications

class LockboxDetailsVC: BasicVC {
    
    // MARK: - Constants
    static let API_REMOVE_DEVICE_URL = ResourceUtil.getApiUrl(key: "remove_box_url")
    static let API_DEVICE_CHANGE_PASS_URL = ResourceUtil.getApiUrl(key: "change_box_password_url")
    static let API_GET_DEVICE_PIN_URL = ResourceUtil.getApiUrl(key: "get_box_pin_url")
    static let API_GET_DEVICE_URL = ResourceUtil.getApiUrl(key: "get_box_url")
    
    // MARK: - Outlets
    
    @IBOutlet weak var topTitleLabel: UILabel!
    
    @IBOutlet weak var deviceNameLabel: UILabel!
    @IBOutlet weak var deviceDescriptionLabel: UILabel!
    @IBOutlet weak var refreshButton: UIButton!
    
    @IBOutlet weak var autoblockSwithcer: UISwitch!
    
    @IBOutlet weak var viewPasscode: UIView!
    @IBOutlet weak var changePasscode: UIView!
    @IBOutlet weak var keypadAutoblock: UIView!
    
    @IBOutlet weak var viewPasscodeLabel: UILabel!
    @IBOutlet weak var changePasscodeLabel: UILabel!
    @IBOutlet weak var autoblockLabel: UILabel!
    @IBOutlet weak var fieldContainer: UIView!
    
    // MARK: - Properties
    
    static let manager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = ResourceUtil.REQUEST_TIMEOUT
        return SessionManager(configuration: configuration)
    }()
    
    var lockbox: Lockbox? {
        didSet {
            fillLabels()
        }
    }
    
    var isDeviceActive = false {
        didSet {
            makeFields(active: isDeviceActive)
        }
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        fillLabels()
        isDeviceActive = lockbox?.state == .locked || lockbox?.state == .unlocked
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        deviceRequest()
        
        AppDelegate.appDidBecomeActiveClojure = {[weak self] in
            DispatchQueue.main.async {
                self?.deviceRequest()
            }
        }
        
        AppDelegate.notificationPermissionChangesClojure = { [weak self] (allowed) in
            DispatchQueue.main.async {
                if let r = self?.refreshButton {
                    r.isHidden = allowed
                }
            }
        }
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            DispatchQueue.main.async {
                self.refreshButton.isHidden = granted
            }
        }
        
        // if passcode was not settled for some reason, it forces to set password
        if let id = self.lockbox?._id, let pass = self.lockbox?.passcode {
            if pass == "" {
                self.goToPasscodeRegistration(with: id)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AppDelegate.notificationPermissionChangesClojure = nil
        AppDelegate.appDidBecomeActiveClojure = nil
    }
    
    private func goToPasscodeRegistration(with id: String) {
        let vc = LockboxRegistrationPasscodeVC()
        vc.lockBoxId = id
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func fillLabels() {
        if deviceNameLabel != nil && deviceNameLabel != nil && topTitleLabel != nil && autoblockSwithcer != nil {
            topTitleLabel.text = lockbox?.name
            deviceNameLabel.text = lockbox?.name
            deviceDescriptionLabel.text = lockbox?.descriptionText
            autoblockSwithcer.isOn = lockbox?.autoLock ?? false
        }
    }

    func makeFields(active: Bool = false) {
        viewPasscode.isUserInteractionEnabled = active
        changePasscode.isUserInteractionEnabled = active
        keypadAutoblock.isUserInteractionEnabled = active
        autoblockSwithcer.isEnabled = active
        if active {
            viewPasscodeLabel.textColor = UIColor.black
            changePasscodeLabel.textColor = UIColor.black
            autoblockLabel.textColor = UIColor.black
        } else {
            viewPasscodeLabel.textColor = UIColor.gray
            changePasscodeLabel.textColor = UIColor.gray
            autoblockLabel.textColor = UIColor.gray
        }
    }
    
    private func deviceWentOffline() {
        let alert = AlertController(
            title: "Autoblock Change Failed",
            message: "The device is offline. Try again later.",
            style: .error)
        alert.add(action: AlertAction(title: "OK", action: {[weak self] in
            self?.isDeviceActive = false
        }))
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Actions
    
    @IBAction func refreshTapped(_ sender: UIButton) {
        deviceRequest()
    }
    
    private func deviceRequest() {
        fieldContainer.isUserInteractionEnabled = false
        
        guard let userId = UserDefaults.standard.string(forKey: "user_id"),
            let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        guard let boxId = lockbox?._id else {
                print("No box data available")
                return
        }
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        let apiUrl = LockboxDetailsVC.API_GET_DEVICE_URL
            .replacingOccurrences(of: "{{user_id}}", with: userId)
            .replacingOccurrences(of: "{{box_id}}", with: boxId)
        
        let waitingAlert = SpinnerController(style: .processing)
        present(waitingAlert, animated: true) {
            LockboxDetailsVC.manager.request(apiUrl,
                                             method: .get,
                                             parameters: nil,
                                             encoding: JSONEncoding.default,
                                             headers: headers)
                .responseJSON { [weak self] response in
                    
                    waitingAlert.dismiss(animated: false) {
                        guard let strongSelf = self else {
                            print("No self reference is available")
                            return
                        }
                        strongSelf.fieldContainer.isUserInteractionEnabled = true
                        let requestTitle = "Device details"
                        // if unauthorized
                        if response.response?.statusCode == 401 {
                            strongSelf.goToSplashScreen()
                            return
                        }
                        // device became offline
                        if response.response?.statusCode == 409 {
                            strongSelf.deviceWentOffline()
                            return
                        }
                        switch response.result {
                        case .success:
                            guard let d = response.data, let regResponseObj = try? JSON(data: d) else {
                                strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                                return
                            }
                            if let status = regResponseObj["status"].string, let msg = regResponseObj["message"].string {
                                strongSelf.showPopUp(title: "Error", message: msg)
                                print("Request finished with status: \(status) and message: \(msg)")
                                return
                            }
                            if regResponseObj["success"].boolValue {
                                guard let strongSelf = self else {
                                    return
                                }
                                
                                strongSelf.lockbox?.id = regResponseObj["box"]["uid"].intValue
                                strongSelf.lockbox?._id = regResponseObj["box"]["_id"].stringValue
                                strongSelf.lockbox?.name = regResponseObj["box"]["name"].stringValue
                                strongSelf.lockbox?.autoLock = regResponseObj["box"]["autoLock"].boolValue
                                strongSelf.lockbox?.descriptionText = regResponseObj["box"]["description"].stringValue
                                strongSelf.lockbox?.passcode = regResponseObj["box"]["password"].stringValue
                                strongSelf.fillLabels()
                                
                                strongSelf.makeFields(active: true)
                            } else {
                                if let errMessage = regResponseObj["error"].string {
                                    strongSelf.showPopUp(title: requestTitle, message: errMessage)
                                } else {
                                    strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                                }
                            }
                        case .failure(let error):
                            if error._code == NSURLErrorTimedOut {
                                strongSelf.showPopUp(title: requestTitle, message: "The Parcel Vault seems to be offline. Please check its Internet connection and try later.")
                            } else {
                                strongSelf.showPopUp(title: requestTitle, message: "Network error")
                            }
                        }
                    }
            }
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func changeName(_ sender: Any) {
        let vc = InputFieldVC(titleText: "Device Name", initialText: lockbox?.name ?? "", type: .singleLine, delegate: self)
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func changeDescription(_ sender: Any) {
        let vc = InputFieldVC(titleText: "Device Description", initialText: lockbox?.descriptionText ?? "", type: .multiLine, delegate: self)
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func viewPasscode(_ sender: Any) {
        
        guard let userId = UserDefaults.standard.string(forKey: "user_id"),
            let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        guard let boxId = lockbox?._id else {
            print("No lock box id is available")
            return
        }
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        let apiUrl = LockboxDetailsVC.API_GET_DEVICE_PIN_URL
            .replacingOccurrences(of: "{{user_id}}", with: userId)
            .replacingOccurrences(of: "{{box_id}}", with: boxId)
        
        if !Connectivity.isConnectedToInternet() {
            showPopUp(title: "Error", message: "The Internet connection appears to be offline")
            return
        }
        
        let waitingAlert = SpinnerController(style: .processing)
        present(waitingAlert, animated: true) {
            DeviceListVC.manager.request(
                apiUrl,
                method: .get,
                parameters: nil,
                encoding: JSONEncoding.default,
                headers: headers)
                .responseJSON { [weak self] response in
                    waitingAlert.dismiss(animated: false) {
                        
                        let requestTitle = "Device pin code"
                        guard let strongSelf = self else {
                            print("No self reference is available")
                            return
                        }
                        // if unauthorized
                        if response.response?.statusCode == 401 {
                            strongSelf.goToSplashScreen()
                            return
                        }
                        
                        // device became offline
                        if response.response?.statusCode == 409 {
                            strongSelf.deviceWentOffline()
                            return
                        }
                        
                        switch response.result {
                        case .success:
                            guard let d = response.data, let regResponseObj = try? JSON(data: d) else {
                                strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                                return
                            }
                            if let status = regResponseObj["status"].string, let msg = regResponseObj["message"].string {
                                strongSelf.showPopUp(title: "Error", message: msg)
                                print("Request finished with status: \(status) and message: \(msg)")
                                return
                            }
                            if regResponseObj["success"].boolValue {
                                strongSelf.lockbox?.passcode = regResponseObj["pin"].stringValue
                                let alert = AlertController(
                                    title: "Parcel Vault Passcode",
                                    message: "Your current passcode: \(regResponseObj["pin"].stringValue)",
                                    style: .default)
                                let okAction = AlertAction(title: "OK", action: {})
                                
                                alert.add(action: okAction)
                                strongSelf.present(alert, animated: true, completion: nil)
                            } else {
                                if let errMessage = regResponseObj["error"].string {
                                    strongSelf.showPopUp(title: requestTitle, message: errMessage)
                                } else {
                                    strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                                }
                            }
                        case .failure(let error):
                            if error._code == NSURLErrorTimedOut {
                                strongSelf.showPopUp(title: requestTitle, message: "The Parcel Vault seems to be offline. Please check its Internet connection and try later.")
                            } else {
                                strongSelf.showPopUp(title: requestTitle, message: "Network error")
                            }
                        }
                    }
            }
        }
    }
    
    @IBAction func changePasscode(_ sender: Any) {
        let vc = PasswordChangingLockboxVC()
        vc.lockbox = lockbox
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func viewHistory(_ sender: Any) {
        let vc = EventHistoryVC()
        vc.lockbox = self.lockbox
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func autoblockTapped(_ sender: UISwitch) {
        autoLockRequest(sender)
    }
    
    @IBAction func autoblockChangedValue(_ sender: UISwitch) {
    }
    
    private func autoLockRequest(_ sender: UISwitch) {
        
        guard let userId = UserDefaults.standard.string(forKey: "user_id"),
            let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        
        guard let boxId = lockbox?._id else {
            print("Parcel Vault ID is unavailable")
            return
        }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        let autoLockData = [
            "autolock": sender.isOn
        ]
        
        let apiUrl = LockboxDetailsVC.API_DEVICE_CHANGE_PASS_URL
            .replacingOccurrences(of: "{{user_id}}", with: userId)
            .replacingOccurrences(of: "{{box_id}}", with: boxId)
        
        if !Connectivity.isConnectedToInternet() {
            showPopUp(title: "Error", message: "The Internet connection appears to be offline")
            return
        }
        
        sender.isEnabled = false
        
        let waitingAlert = SpinnerController(style: .processing)
        present(waitingAlert, animated: true) {
            DeviceListVC.manager.request(apiUrl, method: .put, parameters: autoLockData, encoding: JSONEncoding.default, headers: headers)
                .responseJSON { [weak self] response in
                    waitingAlert.dismiss(animated: false) {
                    sender.isEnabled = true
                    let requestTitle = "AutoBlock option"
                    guard let strongSelf = self else {
                        print("No self reference is available")
                        return
                    }
                    // if unauthorized
                    if response.response?.statusCode == 401 {
                        strongSelf.goToSplashScreen()
                        return
                    }
                    // device became offline
                    if response.response?.statusCode == 409 {
                        strongSelf.deviceWentOffline()
                        return
                    }
                    switch response.result {
                    case .success:
                        guard let d = response.data, let regResponseObj = try? JSON(data: d) else {
                            strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                            return
                        }
                        if let status = regResponseObj["status"].string, let msg = regResponseObj["message"].string {
                            strongSelf.showPopUp(title: "Error", message: msg)
                            print("Request finished with status: \(status) and message: \(msg)")
                            return
                        }
                        if regResponseObj["success"].boolValue {
                            strongSelf.lockbox?.autoLock = regResponseObj["box"]["autoLock"].boolValue
                            
                            sender.isOn = regResponseObj["box"]["autoLock"].boolValue
                            let s = regResponseObj["box"]["autoLock"].boolValue ? "on" : "off"
                            strongSelf.showPopUp(title: "Autoblock Status", message: "The Parcel Vault's keypad autoblock is turned \(s)")
                        } else {
                            if let errMessage = regResponseObj["error"].string {
                                strongSelf.showPopUp(title: requestTitle, message: errMessage)
                            } else {
                                strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                            }
                        }
                    case .failure(let error):
                        if error._code == NSURLErrorTimedOut {
                            sender.isOn = !sender.isOn
                            strongSelf.showPopUp(title: requestTitle, message: "The Parcel Vault seems to be offline. Please check its Internet connection and try later.")
                        } else {
                            strongSelf.showPopUp(title: requestTitle, message: "Network error")
                        }
                    }
                }
            }
        }
        /*
        Alamofire.request(apiUrl,
                          method: .put,
                        parameters: autoLockData,
                        encoding: JSONEncoding.default,
                        headers: headers)
            .responseJSON { [weak self] response in
                sender.isEnabled = true
                let requestTitle = "AutoBlock option"
                guard let strongSelf = self else {
                    print("No self reference is available")
                    return
                }
                // if unauthorized
                if response.response?.statusCode == 401 {
                    strongSelf.goToSplashScreen()
                    return
                }
                // device became offline
                if response.response?.statusCode == 409 {
                    strongSelf.deviceWentOffline()
                    return
                }
                switch response.result {
                case .success:
                    guard let d = response.data, let regResponseObj = try? JSON(data: d) else {
                        strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                        return
                    }
                    if let status = regResponseObj["status"].string, let msg = regResponseObj["message"].string {
                        strongSelf.showPopUp(title: "Error", message: msg)
                        print("Request finished with status: \(status) and message: \(msg)")
                        return
                    }
                    if regResponseObj["success"].boolValue {
                        strongSelf.lockbox?.autoLock = regResponseObj["box"]["autoLock"].boolValue
                        
                        sender.isOn = regResponseObj["box"]["autoLock"].boolValue
                        let s = regResponseObj["box"]["autoLock"].boolValue ? "on" : "off"
                        strongSelf.showPopUp(title: "Autoblock Status", message: "Parcel Vault's keypad autoblock is turned \(s)")
                    } else {
                        if let errMessage = regResponseObj["error"].string {
                            strongSelf.showPopUp(title: requestTitle, message: errMessage)
                        } else {
                            strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                        }
                    }
                case .failure(let error):
                    if error._code == NSURLErrorTimedOut {
                        strongSelf.showPopUp(title: requestTitle, message: "The Parcel Vault seems to be offline. Please check its Internet connection and try later.")
                    } else {
                        strongSelf.showPopUp(title: requestTitle, message: "Network error")
                    }
                }
        }*/
    }
    
    @IBAction func removeDevice(_ sender: Any) {
        let alert = AlertController(
            title: "Parcel Vault Removal",
            message: "Please confirm you wish to remove of the “\(lockbox?.name ?? "Unknown")” device from your account?",
            style: .default)
        let cancelAction = AlertAction(title: "Cancel", action: {})
        let okAction = AlertAction(title: "OK", action: {
            self.removeBoxRequest()
        })
        
        alert.add(action: cancelAction)
        alert.add(action: okAction)
        present(alert, animated: true, completion: nil)
    }
    
    private func removeBoxRequest() {
        guard let userId = UserDefaults.standard.string(forKey: "user_id"),
            let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        
        guard let boxId = lockbox?._id else {
            print("Parent view controller is unavailable")
            return
        }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        
        let apiUrl = LockboxDetailsVC.API_REMOVE_DEVICE_URL
            .replacingOccurrences(of: "{{user_id}}", with: userId)
            .replacingOccurrences(of: "{{box_id}}", with: boxId)
        
        Request.go(vc: self,
                   api: apiUrl,
                   method: .delete,
                   headers: headers,
                   paramData: nil,
                   requestTitle: "Device passcode edit",
                   onSuccessClojure: {[weak self] (json: JSON) in
                    self?.navigationController?.popViewController(animated: true)
        })
    }
}

// MARK: - InputField Delegate

extension LockboxDetailsVC: InputFieldVCDelegate {
    
    func inputField(vc: InputFieldVC, didEnter text: String) {
        updateTextRequest(type: vc.type, text: text)
    }
    
    private func updateTextRequest(type: InputFieldVC.FieldType, text: String) {
        
        guard let userId = UserDefaults.standard.string(forKey: "user_id"),
            let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        
        guard let boxId = lockbox?._id else {
            print("Parent view controller is unavailable")
            return
        }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        let boxData: [String : String]
        if type == .singleLine {
            boxData = ["name": text]
        } else {
            boxData = ["description": text]
        }
        
        let apiUrl = LockboxDetailsVC.API_DEVICE_CHANGE_PASS_URL
            .replacingOccurrences(of: "{{user_id}}", with: userId)
            .replacingOccurrences(of: "{{box_id}}", with: boxId)
        
        Request.go(vc: self,
                   api: apiUrl,
                   method: .put,
                   headers: headers,
                   paramData: boxData,
                   requestTitle: "Device data edit",
                   onSuccessClojure: {[weak self] (json: JSON) in
                    if type == .singleLine {
                        self?.lockbox?.name = json["box"]["name"].stringValue
                    } else {
                        self?.lockbox?.descriptionText = json["box"]["description"].stringValue
                    }
                    self?.fillLabels()
        })
    }
}
