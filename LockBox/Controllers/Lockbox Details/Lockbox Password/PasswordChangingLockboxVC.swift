//
//  PasswordChangingLockboxVC.swift
//  LockBox
//
//  Created by max kryuchkov on 17.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PasswordChangingLockboxVC: BasicVC {
    
    // MARK: - Constants
    static let API_DEVICE_CHANGE_PASS_URL = ResourceUtil.getApiUrl(key: "change_box_password_url")
    
    // MARK: - Outlets
    
    @IBOutlet weak var currentPasswordField: ValidatedTextField!
    @IBOutlet weak var newPasswordField: ValidatedTextField!
    @IBOutlet weak var confirmPasswordField: ValidatedTextField!
    
    // MARK: - Properties
    
    static let manager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = ResourceUtil.REQUEST_TIMEOUT
        return SessionManager(configuration: configuration)
    }()
    
    var lockbox: Lockbox?
    var needShowError = true
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupFields()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        currentPasswordField.subviews.first?.fillSuperview()
        newPasswordField.subviews.first?.fillSuperview()
        confirmPasswordField.subviews.first?.fillSuperview()
    }
    
    // MARK: - Setup
    
    private func setupFields() {
        currentPasswordField.placeholder = "Current Passcode"
        currentPasswordField.secureState = .secure(isTextVisible: false)
        currentPasswordField.textField.keyboardType = .numberPad
        currentPasswordField.delegate = self
        
        newPasswordField.placeholder = "New Passcode"
        newPasswordField.secureState = .secure(isTextVisible: false)
        newPasswordField.textField.keyboardType = .numberPad
        newPasswordField.delegate = self
        
        confirmPasswordField.placeholder = "Confirm New Passcode"
        confirmPasswordField.secureState = .secure(isTextVisible: false)
        confirmPasswordField.textField.keyboardType = .numberPad
        confirmPasswordField.delegate = self
    }
    
    // MARK: - Actions
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        
        guard currentPasswordField.state == .filled else { return }
        guard newPasswordField.state == .filled else { return }
        guard confirmPasswordField.state == .filled else { return }
        if newPasswordField.text == confirmPasswordField.text {
            changePasscodeRequest()
        } else {
            confirmPasswordField.state = .error(message: "The entered passwords do not match.")
        }
    }
    
    private func goToDeviceEdit() {
        let alert = AlertController(
            title: "Success",
            message: "The Parcel Vaults passcode has been changed successfully.",
            style: .default)
        let okAction = AlertAction(title: "OK", action: {
            self.navigationController?.popViewController(animated: true)
        })
        
        alert.add(action: okAction)
        present(alert, animated: true, completion: nil)
    }
    
    private func deviceWentOffline() {
        let alert = AlertController(
            title: "Passcode Change Failed",
            message: "The device is offline. Try again later.",
            style: .error)
        alert.add(action: AlertAction(
            title: "OK",
            action: {[weak self] in
            self?.navigationController?.popViewController(animated: true)
            if let detailsVC = self?.navigationController?.topViewController as? LockboxDetailsVC {
                detailsVC.isDeviceActive = false
            }
        }))
        present(alert, animated: true, completion: nil)
    }
    
    private func changePasscodeRequest() {
        guard let userId = UserDefaults.standard.string(forKey: "user_id"),
            let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        
        guard let boxId = lockbox?._id else {
            print("Parent view controller is unavailable")
            return
        }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        let boxData = [
            "password": newPasswordField.text
        ]
        let apiUrl = PasswordChangingLockboxVC.API_DEVICE_CHANGE_PASS_URL
            .replacingOccurrences(of: "{{user_id}}", with: userId)
            .replacingOccurrences(of: "{{box_id}}", with: boxId)
        
        let waitingAlert = SpinnerController(style: .processing)
        present(waitingAlert, animated: true) {
            PasswordChangingLockboxVC.manager.request(
                apiUrl,
                method: .put,
                parameters: boxData,
                encoding: JSONEncoding.default,
                headers: headers)
                .responseJSON { [weak self] response in
                    
                    waitingAlert.dismiss(animated: false) {
                        let requestTitle = "Change passcode"
                        guard let strongSelf = self else {
                            print("No self reference is available")
                            return
                        }
                        // if unauthorized
                        if response.response?.statusCode == 401 {
                            strongSelf.goToSplashScreen()
                            return
                        }
                        // device became offline
                        if response.response?.statusCode == 409 {
                            strongSelf.deviceWentOffline()
                            return
                        }
                        switch response.result {
                        case .success:
                            guard let d = response.data, let regResponseObj = try? JSON(data: d) else {
                                strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                                return
                            }
                            if let status = regResponseObj["status"].string, let msg = regResponseObj["message"].string {
                                strongSelf.showPopUp(title: "Error", message: msg)
                                print("Request finished with status: \(status) and message: \(msg)")
                                return
                            }
                            if regResponseObj["success"].boolValue {
                                strongSelf.lockbox?.passcode = regResponseObj["box"]["password"].stringValue
                                strongSelf.goToDeviceEdit()
                            } else {
                                if let errMessage = regResponseObj["error"].string {
                                    strongSelf.showPopUp(title: requestTitle, message: errMessage)
                                } else {
                                    strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                                }
                            }
                        case .failure(let error):
                            if error._code == NSURLErrorTimedOut {
                                strongSelf.showPopUp(title: requestTitle, message: "The Parcel Vault seems to be offline. Please check its Internet connection and try later.")
                            } else {
                                strongSelf.showPopUp(title: requestTitle, message: "Network error")
                            }
                        }
                    }
            }
        }
    }
    
}

// MARK: - Validated Field Delegate

extension PasswordChangingLockboxVC: ValidatedTextFieldDelegate {
    
    func validatedStateFor(textField: ValidatedTextField) -> Validator.ValidationState {
        
        if textField == currentPasswordField {
            if lockbox?.passcode != textField.text {
                return .invalid(reason: "Incorrect passcode")
            } else {
                return .valid
            }
        }
        
        if textField == newPasswordField {
            if newPasswordField.text == confirmPasswordField.text {
                confirmPasswordField.state = .filled
            } else {
                confirmPasswordField.state = .error(message: "The entered passwords do not match.")
            }
            return Validator.shared.validate(pinCode: textField.text)
        }
        
        if textField == confirmPasswordField {
            switch Validator.shared.validate(pinCode: newPasswordField.text) {
                case .invalid(let r): newPasswordField.state = .error(message: r)
                default: newPasswordField.state = .filled
            }
            
            if confirmPasswordField.text == newPasswordField.text {
                return .valid
            } else {
                return .invalid(reason: "The entered passcodes do not match.")
            }
        }
        
        return .valid
    }
}
