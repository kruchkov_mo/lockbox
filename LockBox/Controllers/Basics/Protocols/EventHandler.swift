//
//  EventHandler.swift
//  FreeStart
//
//  Created by max kryuchkov on 31.08.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit

protocol EventHandler {}

extension EventHandler {
    
    func showAlertWith(error: Error, controller: UIViewController?, completion: ((UIAlertAction)->())? = nil) {
        showAlertWith(text: error.localizedDescription, controller: controller)
    }
    
    func showAlertWith(text: String, controller: UIViewController?, completion: ((UIAlertAction)->())? = nil) {
        
        let alertVC = UIAlertController(title: nil, message: text, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: completion)
        alertVC.addAction(okAction)
        
        controller?.present(alertVC, animated: true, completion: nil)
    }
    
    func showInfoLabelWithText(_ text: String, controller: UIViewController?, appearanceTime: Double = 0.3, duration: Double = 1.0) {
        
        guard controller != nil else { return }
        
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18.0)
        label.textColor = UIColor.white
        label.text = text
        label.textAlignment = .center
        label.numberOfLines = 0
        label.frame.size = label.sizeThatFits(.init(width: UIWindow.screenSize.width - 50, height: .greatestFiniteMagnitude))
        
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.cornerRadius = 12.0
        view.clipsToBounds = true
        view.frame.size = .init(width: label.frame.width + 16.0, height: label.frame.height + 16.0)
        
        view.addSubview(label)
        label.center = view.center
        
        controller!.view.addSubview(view)
        view.center = .init(x: controller!.view.center.x, y: controller!.view.bounds.height - 32.0 - view.frame.height)
        view.alpha = 0.0
        
        UIView.animate(withDuration: appearanceTime, animations: {
            view.alpha = 1.0
        }, completion: { _ in
            UIView.animate(withDuration: appearanceTime, delay: duration, animations: {
                view.alpha = 0.0
            }, completion: { _ in
                view.removeFromSuperview()
            })
        })
    }
}

