//
//  BasicVC.swift
//  FreeStart
//
//  Created by max kryuchkov on 31.08.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import Alamofire

extension UIViewController {
    
    // MARK: - Navigation
    
    func backDidTap() {
        navigationController?.popViewController(animated: true)
    }
    
    func enableBackSwipeGesture() {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    // MARK: - User Interaction
    
    func disableUserInteractionFor(time: Double) {
        self.view.isUserInteractionEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + time, execute: {
            self.view.isUserInteractionEnabled = true
        })
    }
}

class BasicVC: UIViewController, EventHandler, LoadingVC {
    
    var loadingView: LoadingView?
    
    // MARK: - Initializers
    
    init() {
        super.init(nibName: String(describing: type(of: self)), bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension BasicVC {
    func showPopUp(title t: String, message m: String, style s: AlertController.Style = .default) {
        let alert = AlertController(title: t, message: m, style: s)
        let okAction = AlertAction(title: "Ok", action: {})
        alert.add(action: okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func handleError(error e: Error) -> String {
        var msg = ""
        if let error = e as? AFError {
            switch error {
            case .invalidURL(let url):
                msg = "Invalid URL: \(url) - \(error.localizedDescription)"
                
            case .parameterEncodingFailed(let reason):
                msg = "Parameter encoding failed: \(error.localizedDescription)"
                msg += "\nFailure Reason: \(reason)"
            case .multipartEncodingFailed(let reason):
                msg = "Multipart encoding failed: \(error.localizedDescription)"
                msg += "\nFailure Reason: \(reason)"
            case .responseValidationFailed(let reason):
                msg = "Response validation failed: \(error.localizedDescription)"
                msg += "\nFailure Reason: \(reason)"
                
                switch reason {
                case .dataFileNil, .dataFileReadFailed:
                    msg += "\nDownloaded file could not be read"
                case .missingContentType(let acceptableContentTypes):
                    msg += "\nContent Type Missing: \(acceptableContentTypes)"
                case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                    msg += "\nResponse content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)"
                case .unacceptableStatusCode(let code):
                    msg += "\nResponse status code was unacceptable: \(code)"
                }
            case .responseSerializationFailed(let reason):
                msg = "Response serialization failed: \(error.localizedDescription)"
                msg += "\nFailure Reason: \(reason)"
            }
             msg = "Underlying error: \(error.underlyingError.debugDescription)"
        } else if let error = e as? URLError {
            msg = "URLError occurred: \(error)"
        } else {
            msg = "Unknown error: \(e)"
        }
        return msg
    }
    
    func goToSplashScreen(isEmergency: Bool = true) {
        UserDefaults.standard.removeObject(forKey: "user_id")
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "token")
        if isEmergency {
            let alert = AlertController(
                title: "Authorization error",
                message: "This user account is being currently used on another mobile device",
                style: .error)
            let okAction = AlertAction(title: "Ok", action: {
                let vc = UINavigationController(rootViewController: SplashVC())
                vc.setNavigationBarHidden(true, animated: false)
                AppDelegate.shared.window?.set(toRootViewController: vc)
            })
            alert.add(action: okAction)
            present(alert, animated: true, completion: nil)
        } else {
            let vc = UINavigationController(rootViewController: SplashVC())
            vc.setNavigationBarHidden(true, animated: false)
            AppDelegate.shared.window?.set(toRootViewController: vc)
        }
    }
}
