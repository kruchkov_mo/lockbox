//
//  EventHistoryVC.swift
//  LockBox
//
//  Created by max kryuchkov on 19.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

struct EventHistory {
    var date: String
    var text: String
}

class EventHistoryVC: BasicVC {
    
    // MARK: - Constants
    static let API_DEVICE_HISTORY_URL = ResourceUtil.getApiUrl(key: "box_history_url")
    
    // MARK: - Properties
    var lockbox: Lockbox?
    
    // Pagination
    private var hasMore = false
    private var currentPage = 1
    private let limit = ResourceUtil.getApiInt(key: "box_history_limit")
    
    var history: [EventHistory] = []
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        deviceHistoryRequest()
    }
    
    // MARK: - Setup
    
    private func setupTableView() {
        tableView.registerFromNib(HistoryEventCell.self)
    }
    
    // MARK: - Actions
    @IBAction func backTapp(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    private func deviceHistoryRequest() {
        guard let userId = UserDefaults.standard.string(forKey: "user_id"),
            let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        guard let boxId = lockbox?._id else {
                print("No box id is available")
                return
        }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        
        let apiUrl = EventHistoryVC.API_DEVICE_HISTORY_URL
            .replacingOccurrences(of: "{{user_id}}", with: userId)
            .replacingOccurrences(of: "{{box_id}}", with: boxId)
        
        Request.go(vc: self,
                   api: apiUrl + "?page=\(currentPage)&limit=\(limit)",
                   method: .get,
                   headers: headers,
                   paramData: nil,
                   requestTitle: "Device history",
                   onSuccessClojure: {[weak self] (json: JSON) in
                    guard let strongSelf = self else {
                        return
                    }
                    strongSelf.hasMore = json["has_more"].bool ?? false
                    var extraHistoryItems: [EventHistory] = []
                    for (_, historyEvent) in json["histories"] {
                        let formatedDate = strongSelf.formatDate(date: historyEvent["created_at"].stringValue)
                        let event = EventHistory(date: formatedDate,
                                                 text: historyEvent["description"].stringValue)
                        extraHistoryItems.append(event)
                    }
                    strongSelf.history.append(contentsOf: extraHistoryItems)
                    strongSelf.tableView.reloadData()
        })
    }
    
    private func formatDate(date dateStr: String) -> String {
        let RFC3339DateFormatter = DateFormatter()
        RFC3339DateFormatter.locale = Locale(identifier: "en_US_POSIX")
        RFC3339DateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        RFC3339DateFormatter.timeZone = TimeZone.current
        
        guard let date = RFC3339DateFormatter.date(from: dateStr) else {
            return dateStr
        }
        
        if Calendar.current.isDateInToday(date) || Calendar.current.isDateInYesterday(date) {
            RFC3339DateFormatter.dateFormat = "HH:mm:ss"
        } else {
            RFC3339DateFormatter.dateFormat = "MMM-dd-yyyy HH:mm:ss"
        }
       
        RFC3339DateFormatter.timeZone = TimeZone(identifier: TimeZone.current.abbreviation() ?? "GMT")
        let resultDate = RFC3339DateFormatter.string(from: date)
        
        if Calendar.current.isDateInToday(date) {
            return "Today \(resultDate)"
        } else if Calendar.current.isDateInYesterday(date) {
            return "Yesterday \(resultDate)"
        }
        
        return resultDate
    }
}

// MARK: - TableView DataSource

extension EventHistoryVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return history.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: HistoryEventCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.history = history[safe: indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if hasMore && indexPath.row == history.count - 1 {
            currentPage += 1
            deviceHistoryRequest()
        }
    }
}

// MARK: - TableView Delegate

extension EventHistoryVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return HistoryEventCell.height
    }
    
}
