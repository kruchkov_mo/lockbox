//
//  HistoryEventCell.swift
//  LockBox
//
//  Created by max kryuchkov on 19.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import ConvenientKit

class HistoryEventCell: UITableViewCell, NibLoadableView {
    
    // MARK: - Constants
    
    static let height: CGFloat = 85.0
    
    // MARK: - Outlets
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var historyLabel: UILabel!
    
    // MARK: - Properties
    
    var history: EventHistory? {
        didSet {
            setupViews()
        }
    }
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupViews() {
        dateLabel.text = history?.date
        historyLabel.text = history?.text
    }
    
}
