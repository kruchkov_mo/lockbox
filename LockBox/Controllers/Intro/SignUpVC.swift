//
//  SignUpVC.swift
//  LockBox
//
//  Created by max kryuchkov on 14.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SignUpVC: BasicVC {

    // MARK: - Constants
    static let API_SIGN_UP_URL = ResourceUtil.getApiUrl(key: "sign_up_url")
    
    // MARK: - Properties
    
    // MARK: - Outlets
    
    @IBOutlet weak var emailField: ValidatedTextField!
    @IBOutlet weak var passwordField: ValidatedTextField!
    @IBOutlet weak var confirmField: ValidatedTextField!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupFields()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        emailField.subviews.first?.fillSuperview()
        passwordField.subviews.first?.fillSuperview()
        confirmField.subviews.first?.fillSuperview()
    }
    
    // MARK: - Setup
    
    private func setupFields() {
        emailField.placeholder = "Email"
        emailField.delegate = self
        
        passwordField.placeholder = "Password"
        passwordField.secureState = .secure(isTextVisible: false)
        passwordField.delegate = self
        
        confirmField.placeholder = "Confirm Password"
        confirmField.secureState = .secure(isTextVisible: false)
        confirmField.delegate = self
    }
    
    // MARK: - Actions
    
    @IBAction func signUpTapped(_ sender: Any) {
        
        dismissKeyboard()
        if emailField.state == .blank {
            emailField.state = .error(message: "This field is required")
            return
        }
        if passwordField.state == .blank {
            passwordField.state = .error(message: "This field is required")
            return
        }
        if confirmField.state == .blank {
            confirmField.state = .error(message: "This field is required")
            return
        }
        guard emailField.state == .filled else { return }
        guard passwordField.state == .filled else { return }
        guard confirmField.state == .filled else { return }

        signUpRequest()
    }
    
    private func goToDeviceList() {
        let alert = AlertController(
            title: "Success",
            message: "Your account has been registered successfully.",
            style: .default)
        let okAction = AlertAction(title: "Ok", action: {
            
            let vc = DeviceListVC()
            let navigationVC = UINavigationController(rootViewController: vc)
            navigationVC.setNavigationBarHidden(true, animated: false)
            AppDelegate.shared.window?.set(toRootViewController: navigationVC)
        })
        
        alert.add(action: okAction)
        present(alert, animated: true, completion: nil)
    }
    
    private func signUpRequest() {
        
        if !Connectivity.isConnectedToInternet() {
            showPopUp(title: "Sign up failed", message: "The Internet connection appears to be offline")
            return
        }
        
        guard let token = AppDelegate.deviceToken,
            let vendorId = AppDelegate.currentDeviceUid else {
                self.showPopUp(title: "Sign up failed", message: "Cannot obtain device token for push notifications or vendor id")
                return
        }
 
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-DEVICE-OS": "IOS",
            "X-DEVICE-KEY": vendorId,
            "X-DEVICE-PUSH-TOKEN": token
        ]
        let userCredentials = [
            "email": emailField.text,
            "password": passwordField.text,
            "password_confirmation": confirmField.text
        ]
        
        Request.go(vc: self,
                   api: SignUpVC.API_SIGN_UP_URL,
                   method: .post,
                   headers: headers,
                   paramData: userCredentials,
                   requestTitle: "Sign up",
                   onSuccessClojure: {[weak self] (json: JSON) in
                    UserDefaults.standard.set(json["user"]["_id"].stringValue, forKey: "user_id")
                    UserDefaults.standard.set(json["user"]["email"].stringValue, forKey: "email")
                    UserDefaults.standard.set(json["user"]["token"].stringValue, forKey: "token")
                    self?.goToDeviceList()
        })
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Data
    
    
}

extension SignUpVC: ValidatedTextFieldDelegate {
    
    func validatedStateFor(textField: ValidatedTextField) -> Validator.ValidationState {
        
        if textField == emailField {
            return Validator.shared.validate(email: emailField.text)
        }
        
        if textField == passwordField {
            return Validator.shared.validate(password: passwordField.text)
        }
        
        if textField == confirmField {
            if passwordField.text == confirmField.text {
                return .valid
            } else {
                return .invalid(reason: "The entered passwords do not match.")
            }
        }
        
        return .valid
    }
    
}
