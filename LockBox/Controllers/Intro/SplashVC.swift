//
//  SplashVC.swift
//  LockBox
//
//  Created by max kryuchkov on 14.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit

class SplashVC: BasicVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Is user logged in
        if UserDefaults.standard.object(forKey: "user_id") != nil {
            let navigationVC = UINavigationController(rootViewController: DeviceListVC())
            navigationVC.setNavigationBarHidden(true, animated: false)
            AppDelegate.shared.window?.set(toRootViewController: navigationVC)
            return
        }
        
        navigationController?.pushViewController(LoginVC(), animated: false)
    }
}
