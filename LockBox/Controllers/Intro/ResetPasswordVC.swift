//
//  ResetPasswordVC.swift
//  LockBox
//
//  Created by max kryuchkov on 14.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ResetPasswordVC: BasicVC {

    // MARK: - Constants
    static let API_RESET_URL = ResourceUtil.getApiUrl(key: "reset_user_pass_url")
    
    // MARK: - Properties
    
    // MARK: - Outlets
    
    @IBOutlet weak var emailField: ValidatedTextField!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupFields()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        emailField.subviews.first?.fillSuperview()
    }
    
    // MARK: - Setup
    
    private func setupFields() {
        emailField.placeholder = "Email"
        emailField.delegate = self
    }
    
    // MARK: - Actions
    
    @IBAction func resetTapped(_ sender: Any) {
        dismissKeyboard()
        
        if emailField.state == .blank {
            emailField.state = .error(message: "This field is required")
            return
        }
        
        guard emailField.state == .filled else { return }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        let lockBoxData = [
            "email": emailField.text,
            ]
        
        Request.go(vc: self,
                   api: ResetPasswordVC.API_RESET_URL,
                   method: .post,
                   headers: headers,
                   paramData: lockBoxData,
                   requestTitle: "Reset password",
                   onSuccessClojure: {[weak self] (json: JSON) in
                   self?.goToSignIn()
        })
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    private func goToSignIn() {
        let alert = AlertController(
            title: "Reset Password",
            message: "The email with the reset password request has been sent to \(emailField.text)",
            style: .default)
        let okAction = AlertAction(title: "Ok", action: { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        })
        
        alert.add(action: okAction)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Data
}

extension ResetPasswordVC: ValidatedTextFieldDelegate {
    
    func validatedStateFor(textField: ValidatedTextField) -> Validator.ValidationState {
        
        if textField == emailField {
            return Validator.shared.validate(email: emailField.text)
        }
        
        return .valid
    }
    
}
