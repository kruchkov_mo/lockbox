//
//  LoginVC.swift
//  LockBox
//
//  Created by max kryuchkov on 14.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class LoginVC: BasicVC {

    // MARK: - Constants
    static let API_LOGIN_URL = ResourceUtil.getApiUrl(key: "login_url")
    
    // MARK: - Properties
    
    // MARK: - Outlets
    
    @IBOutlet weak var emailField: ValidatedTextField!
    @IBOutlet weak var passwordField: ValidatedTextField!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupFields()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        passwordField.text = ""
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        passwordField.subviews.first?.fillSuperview()
        emailField.subviews.first?.fillSuperview()
    }
    
    // MARK: - Setup

    private func setupFields() {
        emailField.placeholder = "Email"
        emailField.delegate = self
        
        passwordField.placeholder = "Password"
        passwordField.secureState = .secure(isTextVisible: false)
        passwordField.delegate = self
    }
    
    // MARK: - Actions
    
    @IBAction func loginTapped(_ sender: Any) {
        dismissKeyboard()
        if emailField.state == .blank {
            emailField.state = .error(message: "This field is required")
            return
        }
        if passwordField.state == .blank {
            passwordField.state = .error(message: "This field is required")
            return
        }
        guard emailField.state == .filled else { return }
        guard passwordField.state == .filled else { return }
        
        guard let token = AppDelegate.deviceToken else {
                self.showPopUp(title: "Log in failed", message: "Cannot obtain device token for push notifications")
                return
        }
        guard let vendorId = AppDelegate.currentDeviceUid else {
                self.showPopUp(title: "Log in failed", message: "Cannot obtain device vendor id")
                return
        }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-DEVICE-OS": "IOS",
            "X-DEVICE-KEY": vendorId,
            "X-DEVICE-PUSH-TOKEN": token
        ]
        let userCredentials = [
            "email": emailField.text,
            "password": passwordField.text
        ]
        Request.go(vc: self,
                   api: LoginVC.API_LOGIN_URL,
                   method: .post,
                   headers: headers,
                   paramData: userCredentials,
                   requestTitle: "Authentication",
                   onSuccessClojure: {[weak self] (json: JSON) in
                    UserDefaults.standard.set(json["user"]["_id"].stringValue, forKey: "user_id")
                    UserDefaults.standard.set(json["user"]["email"].stringValue, forKey: "email")
                    UserDefaults.standard.set(json["user"]["token"].stringValue, forKey: "token")
                    self?.goToDeviceListVC()
        })
    }
    
    private func goToDeviceListVC() {
        let vc = DeviceListVC()
        let navigationVC = UINavigationController(rootViewController: vc)
        navigationVC.setNavigationBarHidden(true, animated: false)
        AppDelegate.shared.window?.set(toRootViewController: navigationVC)
    }
    
    @IBAction func forgotTapped(_ sender: Any) {
        navigationController?.pushViewController(ResetPasswordVC(), animated: true)
    }
    
    @IBAction func signUpTapped(_ sender: Any) {
        navigationController?.pushViewController(SignUpVC(), animated: true)
    }
    
    // MARK: - Data
    
}

extension LoginVC: ValidatedTextFieldDelegate {
    
    func validatedStateFor(textField: ValidatedTextField) -> Validator.ValidationState {
        
        if textField == emailField {
            return Validator.shared.validate(email: emailField.text)
        }
        
        if textField == passwordField {
            return Validator.shared.validate(password: passwordField.text)
        }
        
        return .valid
    }
    
}
