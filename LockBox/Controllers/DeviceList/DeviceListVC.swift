//
//  DeviceListVC.swift
//  LockBox
//
//  Created by max kryuchkov on 15.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import UserNotifications

class DeviceListVC: BasicVC {
    
    // MARK: - Constants
    static let API_DEVICE_LIST_URL = ResourceUtil.getApiUrl(key: "device_list_url")
    static let API_GET_DEVICE_URL = ResourceUtil.getApiUrl(key: "get_box_url")
    static let SYNC_INTERVAL = TimeInterval(ResourceUtil.getApiInt(key: "sync_interval_sec"))  // 5 min
    
    // MARK: - Properties
    
    static let manager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = ResourceUtil.REQUEST_TIMEOUT
        return SessionManager(configuration: configuration)
    }()
    
    var lockboxes: [Lockbox] = []
    
    
    var syncTimer: Timer?
    
    var isSyncTimer = false {
        didSet {
            if isSyncTimer {
                syncTimer = Timer.scheduledTimer(withTimeInterval: DeviceListVC.SYNC_INTERVAL, repeats: true) { [weak self] timer in
                    self?.syncRequest()
                }
            } else {
                if syncTimer != nil {
                    syncTimer?.invalidate()
                    syncTimer = nil
                }
            }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet weak var devicesTableView: UITableView!
    @IBOutlet weak var tableFooter: UIView!
    @IBOutlet weak var refreshButton: UIButton!
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppDelegate.appDidBecomeActiveClojure = {[weak self] in
            DispatchQueue.main.async {
                self?.deviceListRequest()
            }
        }
        
        AppDelegate.notificationPermissionChangesClojure = {[weak self] (allowed) in
            DispatchQueue.main.async {
                if let r = self?.refreshButton {
                    r.isHidden = allowed
                }
            }
        }
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            DispatchQueue.main.async {
                self.refreshButton.isHidden = granted
            }
        }
        deviceListRequest()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AppDelegate.notificationPermissionChangesClojure = nil
        AppDelegate.appDidBecomeActiveClojure = nil
        // dispoose timer
        isSyncTimer = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    // MARK: - Setup
    
    @objc
    private func syncRequest() {
        guard let userId = UserDefaults.standard.string(forKey: "user_id"),
            let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        let apiUrl = DeviceListVC.API_DEVICE_LIST_URL.replacingOccurrences(of: "{{user_id}}", with: userId)
        
        Request.go(vc: self,
                   api: apiUrl,
                   method: .get,
                   headers: headers,
                   paramData: nil,
                   requestTitle: "Synchronize request",
                   onSuccessClojure: {[weak self] (json: JSON) in
                    guard let strongSelf = self else {
                        return
                    }
                    
                    strongSelf.lockboxes = []
                    
                    var isSync = false
                    for (_, box) in json["boxes"] {
                        let lockBox = Lockbox()
                        lockBox.id = box["uid"].intValue
                        lockBox._id = box["_id"].stringValue
                        lockBox.name = box["name"].stringValue
                        lockBox.autoLock = box["autoLock"].boolValue
                        lockBox.descriptionText = box["description"].stringValue
                        lockBox.passcode = box["password"].stringValue
                        print("sync request: \(box)")
                        if box["status"].boolValue {
                            let remainingTime = box["remaining_time"].int ?? -1
                            if remainingTime > 0 {
                                lockBox.state = .willBeAvailableAfter(timestamp: TimeInterval(remainingTime))
                                isSync = true
                            } else {
                                lockBox.state = box["locked"].boolValue ? .locked : .unlocked
                            }
                        } else {
                            lockBox.state = .offline
                        }
                        strongSelf.lockboxes.append(lockBox)
                    }
                    if strongSelf.isSyncTimer != isSync {
                        strongSelf.isSyncTimer = isSync
                    }
                    strongSelf.devicesTableView.reloadData()
        })
    }
    
    private func deviceListRequest() {
        
        let requestTitle = "Device list"
        if !Connectivity.isConnectedToInternet() {
            showPopUp(title: requestTitle, message: "The Internet connection appears to be offline")
            return
        }
        
        guard let userId = UserDefaults.standard.string(forKey: "user_id"),
            let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        let apiUrl = DeviceListVC.API_DEVICE_LIST_URL.replacingOccurrences(of: "{{user_id}}", with: userId)
        
        let waitingAlert = SpinnerController(style: .processing)
        
        let presentClosure = {
        self.present(waitingAlert, animated: true) {
            DeviceListVC.manager.request(apiUrl,
                                         method: .get,
                                         parameters: nil,
                                         encoding: JSONEncoding.default,
                                         headers: headers)
                .responseJSON { [weak self] response in
                    
                    waitingAlert.dismiss(animated: false) {
                        guard let strongSelf = self else {
                            print("No self reference is available")
                            return
                        }
                        // if unauthorized
                        if response.response?.statusCode == 401 {
                            strongSelf.goToSplashScreen()
                            return
                        }
                        
                        switch response.result {
                        case .success:
                            guard let d = response.data, let regResponseObj = try? JSON(data: d) else {
                                strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                                return
                            }
                            if let status = regResponseObj["status"].string, let msg = regResponseObj["message"].string {
                                strongSelf.showPopUp(title: "Error", message: msg)
                                print("Request finished with status: \(status) and message: \(msg)")
                                return
                            }
                            if regResponseObj["success"].boolValue {
                                strongSelf.lockboxes = []
                                var isSync = false
                                
                                for (_, box) in regResponseObj["boxes"] {
                                    let lockBox = Lockbox()
                                    lockBox.id = box["uid"].intValue
                                    lockBox._id = box["_id"].stringValue
                                    lockBox.name = box["name"].stringValue
                                    lockBox.autoLock = box["autoLock"].boolValue
                                    lockBox.descriptionText = box["description"].stringValue
                                    lockBox.passcode = box["password"].stringValue
                                    
                                    print("dev list request: \(box)")
                                    if box["status"].boolValue {
                                        let remainingTime = box["remaining_time"].int ?? -1
                                        if remainingTime > 0 {
                                            lockBox.state = .willBeAvailableAfter(timestamp: TimeInterval(remainingTime))
                                            isSync = true
                                        } else {
                                            lockBox.state = box["locked"].boolValue ? .locked : .unlocked
                                        }
                                    } else {
                                        lockBox.state = .offline
                                    }
                                    
                                    strongSelf.lockboxes.append(lockBox)
                                }
                                if strongSelf.isSyncTimer != isSync {
                                    strongSelf.isSyncTimer = isSync
                                }
                                strongSelf.devicesTableView.reloadData()
                            } else {
                                if let errMessage = regResponseObj["error"].string {
                                    strongSelf.showPopUp(title: requestTitle, message: errMessage)
                                } else {
                                    strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                                }
                            }
                        case .failure(let error):
                            if error._code == NSURLErrorTimedOut {
                                strongSelf.showPopUp(title: requestTitle, message: "The Parcel Vault seems to be offline. Please check its Internet connection and try later.")
                            } else {
                                strongSelf.showPopUp(title: requestTitle, message: "Network error")
                            }
                        }
                    }
            }
        }
        }   // present closure
      
        if let presentedVC = self.presentedViewController as? SpinnerController {
            presentedVC.dismiss(animated: false) {
                presentClosure()
            }
        } else {
            presentClosure()
        }
    }
    
    private func setupTableView() {
        devicesTableView.registerFromNib(LockboxTVCell.self)
        devicesTableView.tableFooterView = tableFooter
    }
    
    // MARK: - Actions
    
    @IBAction func refreshTapped(_ sender: UIButton) {
        deviceListRequest()
    }
    
    @IBAction func createLockbox(_ sender: Any) {
        navigationController?.pushViewController(LockboxRegistrationVC(), animated: true)
    }
    
    @IBAction func settingsTapped(_ sender: Any) {
        navigationController?.pushViewController(AccountSettingsVC(), animated: true)
    }
    
    // MARK: - Data
    
    func updateItem(by id: String, _ event: String) {
        let requestTitle = "Device list item update"
        
        if !Connectivity.isConnectedToInternet() {
            showPopUp(title: requestTitle, message: "The Internet connection appears to be offline")
            return
        }
        
        if event == "OFFLINE" {
            let i = getIndexBy(id: id)
            if i != -1 {
                lockboxes[i].state = .offline
                devicesTableView.reloadData()
            }
            return
        }
        
        if event == "KEYPAD_LOCKED" {
            deviceListRequest()
            return
        }
     
        guard let userId = UserDefaults.standard.string(forKey: "user_id"),
            let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        let apiUrl = DeviceListVC.API_GET_DEVICE_URL
            .replacingOccurrences(of: "{{user_id}}", with: userId)
            .replacingOccurrences(of: "{{box_id}}", with: id)
        
        let waitingAlert = SpinnerController(style: .processing)
        self.present(waitingAlert, animated: true) {
            DeviceListVC.manager.request(apiUrl,
                                         method: .get,
                                         parameters: nil,
                                         encoding: JSONEncoding.default,
                                         headers: headers)
                .responseJSON { [weak self] response in
                    
                    waitingAlert.dismiss(animated: false) {
                        guard let strongSelf = self else {
                            print("No self reference is available")
                            return
                        }
                        
                        // if unauthorized
                        if response.response?.statusCode == 401 {
                            strongSelf.goToSplashScreen()
                            return
                        }
                        
                        switch response.result {
                        case .success:
                            guard let d = response.data, let regResponseObj = try? JSON(data: d) else {
                                strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                                return
                            }
                            if let status = regResponseObj["status"].string, let msg = regResponseObj["message"].string {
                                strongSelf.showPopUp(title: "Error", message: msg)
                                print("Request finished with status: \(status) and message: \(msg)")
                                return
                            }
                            if regResponseObj["success"].boolValue {
                                
                                print("item request: \(regResponseObj["box"])")
                                let i = strongSelf.getIndexBy(id: id)
                                if i != -1 {
                                    
                                    if regResponseObj["box"]["status"].boolValue {
                                        let remainingTime = regResponseObj["box"]["remaining_time"].int ?? -1
                                        
                                        if remainingTime > 0 {
                                            strongSelf.lockboxes[i].state = .willBeAvailableAfter(timestamp: TimeInterval(remainingTime))
                                            if strongSelf.isSyncTimer == false {
                                                strongSelf.isSyncTimer = true
                                            }
                                        } else {
                                            strongSelf.lockboxes[i].state = regResponseObj["box"]["locked"].boolValue ? .locked : .unlocked
                                        }
                                    } else {
                                        strongSelf.lockboxes[i].state = .offline
                                    }
                                    
                                    strongSelf.devicesTableView.reloadData()
                                }
                            } else {
                                if let errMessage = regResponseObj["error"].string {
                                    strongSelf.showPopUp(title: requestTitle, message: errMessage)
                                } else {
                                    strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                                }
                            }
                        case .failure(let error):
                            if error._code == NSURLErrorTimedOut {
                                strongSelf.showPopUp(title: requestTitle, message: "The Parcel Vault seems to be offline. Please check its Internet connection and try later.")
                            } else {
                                strongSelf.showPopUp(title: requestTitle, message: "Network error")
                            }
                        }
                    }
            }
        }
    }
    
    private func getIndexBy(id: String) -> Int {
        for (index, box) in lockboxes.enumerated() {
            if box._id == id {
                return index
            }
        }
        return -1
    }
    
}

// MARK: - TableView DataSource

extension DeviceListVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lockboxes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: LockboxTVCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.lockbox = lockboxes[safe: indexPath.row]
        cell.vc = self
        
        return cell
    }
}

// MARK: - TableView Delegate

extension DeviceListVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return LockboxTVCell.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
       
        if let lockbox = lockboxes[safe: indexPath.row] {
            let vc = LockboxDetailsVC()
            vc.lockbox = lockbox
                
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
