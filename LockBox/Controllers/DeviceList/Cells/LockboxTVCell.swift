//
//  LockboxTVCell.swift
//  LockBox
//
//  Created by max kryuchkov on 15.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import ConvenientKit
import Alamofire
import SwiftyJSON

class LockboxTVCell: UITableViewCell, NibLoadableView {
    
    // MARK: - Constants
    static let API_DEVICE_CHANGE_LOCK_URL = ResourceUtil.getApiUrl(key: "device_lock_url")
    static let UPDATE_INTERVAL: TimeInterval = 0.5

    static let height: CGFloat = 94.0

    // MARK: - Outlets

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var stateLabel: UILabel!
    
    @IBOutlet weak var switcher: CustomSwitch! {
        didSet {
            setupSwitcher()
        }
    }
    
    // MARK: - Properties

    weak var vc: BasicVC?
    
    var lockbox: Lockbox? {
        didSet {
            setupViews()
        }
    }
    
    /*
    // Timer stuff
    private var endDate: Date?
    private weak var currentTimer: Timer?
    var expiryTimeInterval: TimeInterval? {
        didSet {
            if (expiryTimeInterval ?? 0.0) > 0.0 {
                startTimer()
            }
        }
    }
    
    private func stopTimerIfRunning() {
        if currentTimer != nil {
            currentTimer?.invalidate()
            currentTimer = nil
        }
    }
    
    private func startTimer() {
        if let interval = expiryTimeInterval {
            endDate = Date(timeIntervalSinceNow: interval)
            stopTimerIfRunning()
            currentTimer = Timer.scheduledTimer(withTimeInterval: LockboxTVCell.UPDATE_INTERVAL, repeats: true) { [weak self] timer in
                if let strongSelf = self, let end = strongSelf.endDate {
                    let diff = Int(end.timeIntervalSince1970 - Date().timeIntervalSince1970)
                    if diff >= 0 {
                        strongSelf.stateLabel?.text = strongSelf.timeIntervalToFormattedString(TimeInterval(diff))
                        return
                    }
                }
                timer.invalidate()
                if let parent = self?.vc as? DeviceListVC, let deviceId = self?.lockbox?._id {
                    parent.updateItem(by: deviceId, "")
                }
            }
        }
    }
    */
    
    // MARK: - Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupViews() {
        nameLabel?.text = lockbox?.name
        nameLabel?.textColor = UIColor.black
        
        //stopTimerIfRunning()
        switch (lockbox?.state ?? .offline) {
        case .unlocked:
            iconView?.image = #imageLiteral(resourceName: "lockboxSmallActive")
            stateLabel?.text = ""
            switcher?.isHidden = false
            switcher?.isOn = true
        case .locked:
            iconView?.image = #imageLiteral(resourceName: "lockboxSmallActive")
            stateLabel?.text = ""
            switcher?.isHidden = false
            switcher?.isOn = false
        case .offline:
            iconView?.image = #imageLiteral(resourceName: "lockboxSmallInactive")
            stateLabel?.text = "Offline"
            nameLabel?.textColor = UIColor.gray
            switcher?.isHidden = true
        case .willBeAvailableAfter(let timestamp):
            iconView?.image = #imageLiteral(resourceName: "lockboxSmallActive")
            stateLabel?.text = timeIntervalToFormattedString(timestamp)
            // Start timer
            //expiryTimeInterval = timestamp
            switcher?.isHidden = true
        }
        switcher?.setupLabels()
    }
    
    private func setupSwitcher() {
        
        let makeLabel: (String) -> (UILabel) = { title in
            let result = UILabel()
            result.font = UIFont.systemFont(ofSize: 14.0, weight: .medium)
            result.textAlignment = .center
            result.textColor = UIColor.white
            result.lineBreakMode = .byTruncatingTail
            result.text = title
            result.sizeToFit()
            return result
        }
        switcher?.cornerRadiusS = switcher.frame.height / 2.0
        switcher?.onTintColor = UIColor(colorWithHexValue: 0xFAB800)
        switcher?.offTintColor = UIColor(colorWithHexValue: 0xAEAEAE)
        switcher?.labelOn = makeLabel("UNLOCKED")
        switcher?.labelOff = makeLabel("LOCKED")
        
        switcher?.areLabelsShown = true
    }
    
    @IBAction func switchTouched(_ sender: CustomSwitch) {
        let switchState = sender.isOn
        guard let userId = UserDefaults.standard.string(forKey: "user_id"),
            let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        
        guard let strongVC = vc, let boxId = lockbox?._id else {
            print("Parent view controller is unavailable")
            return
        }
        
        let cmd = sender.isOn ? "unlock" : "lock"
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        let apiUrl = LockboxTVCell.API_DEVICE_CHANGE_LOCK_URL
            .replacingOccurrences(of: "{{user_id}}", with: userId)
            .replacingOccurrences(of: "{{box_id}}", with: boxId)
            .replacingOccurrences(of: "{{lock_state}}", with: cmd)

        if !Connectivity.isConnectedToInternet() {
            strongVC.showPopUp(title: "Error", message: "The Internet connection appears to be offline")
            return
        }
        
        sender.isEnabled = false
        
        let waitingAlert = SpinnerController(style: .processing)
        strongVC.present(waitingAlert, animated: true) {
            DeviceListVC.manager.request(
                apiUrl,
                method: .patch,
                parameters: nil,
                encoding: JSONEncoding.default,
                headers: headers)
                .responseJSON { [weak strongVC] response in
                    
                    waitingAlert.dismiss(animated: false) {
                        sender.isEnabled = true
                        let requestTitle = "Device lock"
                        guard let strongSelf = strongVC else {
                            print("No self reference is available")
                            return
                        }
                        // if unauthorized
                        if response.response?.statusCode == 401 {
                            strongSelf.goToSplashScreen()
                            return
                        }
                        var newState = !switchState
                        if response.response?.statusCode == 400 {
                            if let dat = response.data, let obj = try? JSON(data: dat), let msg = obj["message"].string {
                               strongSelf.showPopUp(title: requestTitle, message: msg)
                            } else {
                                strongSelf.showPopUp(title: requestTitle, message: "Device is not available at the moment")
                            }
                            sender.setOn(on: !switchState, animated: true)
                            return
                        }
                        switch response.result {
                        case .success:
                            guard let d = response.data, let regResponseObj = try? JSON(data: d) else {
                                strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                                return
                            }
                            if let status = regResponseObj["status"].string, let msg = regResponseObj["message"].string {
                                strongSelf.showPopUp(title: "Error", message: msg)
                                print("Request finished with status: \(status) and message: \(msg)")
                                return
                            }
                            if regResponseObj["success"].boolValue {
                                newState = !regResponseObj["box"]["locked"].boolValue
                                let lockStr = regResponseObj["box"]["locked"].boolValue ? "locked" : "unlocked"
                                let parcelVaultName = self.lockbox?.name ?? "Parcel Vault"
                                strongSelf.showPopUp(title: "Status", message: "\(parcelVaultName) was manually \(lockStr).")
                            } else {
                                if let errMessage = regResponseObj["error"].string {
                                    strongSelf.showPopUp(title: requestTitle, message: errMessage)
                                } else {
                                    strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                                }
                            }
                        case .failure(let error):
                            if error._code == NSURLErrorTimedOut {
                                strongSelf.showPopUp(title: requestTitle, message: "The Parcel Vault seems to be offline. Please check its Internet connection and try later.")
                            } else {
                                strongSelf.showPopUp(title: requestTitle, message: "Network error")
                            }
                        }
                        
                        sender.setOn(on: newState, animated: true)
                    }
            }
        }
    }
    
    private func timeIntervalToFormattedString(_ timeInterval: TimeInterval) -> String {
        
        if timeInterval == 0 {
            return "Just now"
        }
        
        var mutableInterval = timeInterval
        let hours = Int((mutableInterval / (60.0 * 60.0)).rounded(.towardZero))
        mutableInterval = mutableInterval.truncatingRemainder(dividingBy: 60.0 * 60.0)
        let minutes = Int((mutableInterval / 60.0).rounded(.towardZero))
        mutableInterval = mutableInterval.truncatingRemainder(dividingBy: 60.0)
        //let seconds = Int(mutableInterval)
        
        var resultString = ""
        if hours > 0 {
            resultString += "\(hours)h "
        }
        if minutes >= 0 {
            resultString += "\(minutes)min "
        }
        /*
        if hours == 0, seconds >= 0 {
            resultString += "\(seconds)sec"
        }
        */
        return resultString
    }
    
}
