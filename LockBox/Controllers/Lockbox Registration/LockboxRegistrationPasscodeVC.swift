//
//  LockboxRegistrationPasscodeVC.swift
//  LockBox
//
//  Created by max kryuchkov on 17.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LockboxRegistrationPasscodeVC: BasicVC {
    
    @IBOutlet weak var passcodeField: ValidatedTextField!
    @IBOutlet weak var confirmField: ValidatedTextField!

    static let API_LOCKBOX_PASS_REG_URL = ResourceUtil.getApiUrl(key: "lockbox_reg_pass_url")
    //static let API_LOCKBOX_STATUS_URL = ResourceUtil.getApiUrl(key: "box_lock_status_url")
    
    var lockBoxId: String?
    
    static let manager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = ResourceUtil.REQUEST_TIMEOUT
        return SessionManager(configuration: configuration)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFields()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        passcodeField.subviews.first?.fillSuperview()
        confirmField.subviews.first?.fillSuperview()
    }
    
    // MARK: - Setup
    
    private func setupFields() {
        passcodeField.placeholder = "Passcode"
        passcodeField.secureState = .secure(isTextVisible: false)
        passcodeField.textField.keyboardType = .numberPad
        passcodeField.delegate = self
        
        confirmField.placeholder = "Confirm Passcode"
        confirmField.secureState = .secure(isTextVisible: false)
        confirmField.textField.keyboardType = .numberPad
        confirmField.delegate = self
    }
    
    
    // MARK: - Actions
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        guard passcodeField.state == .filled else { return }
        guard confirmField.state == .filled else { return }
        
        if passcodeField.text == confirmField.text {
            regPasscodeRequest()
        } else {
            confirmField.state = .error(message: "The entered passcodes do not match.")
        }
    }
    
    private func goToRootVC() {
        let alert = AlertController(
            title: "Parcel Vault Registration",
            message: "The Parcel Vault device has been registered successfully.",
            style: .default)
        let okAction = AlertAction(title: "Ok", action: {
            self.navigationController?.popToRootViewController(animated: true)
        })
        
        alert.add(action: okAction)
        present(alert, animated: true, completion: nil)
    }
    
    private func regPasscodeRequest() {
        guard let boxId = lockBoxId else {
            print("No lock box id is available")
            return
        }
        guard let userId = UserDefaults.standard.string(forKey: "user_id"),
            let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        let passcodeData = [
            "password": passcodeField.text
        ]
        
        let apiUrl = LockboxRegistrationPasscodeVC.API_LOCKBOX_PASS_REG_URL
            .replacingOccurrences(of: "{{user_id}}", with: userId)
            .replacingOccurrences(of: "{{box_id}}", with: boxId)
        
        let waitingAlert = SpinnerController(style: .processing)
        present(waitingAlert, animated: true) {
            LockboxRegistrationPasscodeVC.manager.request(
                apiUrl,
                method: .put,
                parameters: passcodeData,
                encoding: JSONEncoding.default,
                headers: headers)
                .responseJSON { [weak self] response in
                    
                    waitingAlert.dismiss(animated: false) {
                        let requestTitle = "Passcode registration"
                        guard let strongSelf = self else {
                            print("No self reference is available")
                            return
                        }
                        // if unauthorized
                        if response.response?.statusCode == 401 {
                            strongSelf.goToSplashScreen()
                            return
                        }
                        switch response.result {
                        case .success:
                            guard let d = response.data, let regResponseObj = try? JSON(data: d) else {
                                strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                                return
                            }
                            if let status = regResponseObj["status"].string, let msg = regResponseObj["message"].string {
                                strongSelf.showPopUp(title: "Error", message: msg)
                                print("Request finished with status: \(status) and message: \(msg)")
                                return
                            }
                            if regResponseObj["success"].boolValue {
                                //strongSelf.lockBoxLockStatusRequest()
                                strongSelf.goToRootVC()
                            } else {
                                if let errMessage = regResponseObj["error"].string {
                                    strongSelf.showPopUp(title: requestTitle, message: errMessage)
                                } else {
                                    strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                                }
                            }
                        case .failure(let error):
                            if error._code == NSURLErrorTimedOut {
                                strongSelf.showPopUp(title: requestTitle, message: "The Parcel Vault seems to be offline. Please check its Internet connection and try later.")
                            } else {
                                strongSelf.showPopUp(title: requestTitle, message: "Network error")
                            }
                        }
                    }
            }
        }
    }
    
}

// MARK: - Validated Field Delegate

extension LockboxRegistrationPasscodeVC: ValidatedTextFieldDelegate {
 
    func validatedStateFor(textField: ValidatedTextField) -> Validator.ValidationState {
        
        if textField == passcodeField {
            
            if passcodeField.text == confirmField.text {
                confirmField.state = .filled
            } else {
                confirmField.state = .error(message: "The entered passwords do not match.")
            }
            
            return Validator.shared.validate(pinCode: passcodeField.text)
        }
        
        if textField == confirmField {
            switch Validator.shared.validate(pinCode: passcodeField.text) {
                case .invalid(let r): passcodeField.state = .error(message: r)
                default: passcodeField.state = .filled
            }
            if passcodeField.text == confirmField.text {
                return .valid
            } else {
                return .invalid(reason: "The entered passwords do not match.")
            }
        }
        
        
        return .valid
    }
}



