//
//  LockboxRegistrationVC.swift
//  LockBox
//
//  Created by max kryuchkov on 15.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LockboxRegistrationVC: BasicVC {
    // MARK: - Constants
    static let API_LOCKBOX_REG_URL = ResourceUtil.getApiUrl(key: "lockbox_reg_url")

    // MARK: - Outlets

    @IBOutlet weak var nameField: ValidatedTextField!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var idField: ValidatedTextField!
    @IBOutlet weak var descValue: UILabel!
    
    // MARK: - Properties
    
    var isDescriptionChanged = false

    private var initText: String = ""
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupFields()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        nameField.subviews.first?.fillSuperview()
        idField.subviews.first?.fillSuperview()
    }
    
    // MARK: - Setup
    
    private func setupFields() {
        nameField.placeholder = "Device Name"
        nameField.delegate = self
        
        idField.placeholder = "Type the Parcel Vault ID"
        idField.textField.autocapitalizationType = UITextAutocapitalizationType.allCharacters
        //idField.textField.keyboardType = .numberPad
        idField.delegate = self
        descValue.text = "Value..."
    }


    // MARK: - Actions

    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func enterDescriptionTapped(_ sender: Any) {
        let vc = InputFieldVC(titleText: "Description", initialText: initText, type: .multiLine, delegate: self)
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func registerTapped(_ sender: Any) {
        guard nameField.state == .filled else { return }
        
        //guard isDescriptionChanged, !(descriptionLabel.text ?? "").isEmpty else { return }
        guard idField.state == .filled else { return }
        
        if nameField.text.count < 4 {
            self.showPopUp(
                title: "Device registration",
                message: "\"name\" length must be at least 4 characters long",
                style: .error)
            return
        }
        
        lockBoxRegistrationRequest(sender: sender as! UIButton)
    }
    
    private func goToPasscodeRegistration(with id: String) {
        let vc = LockboxRegistrationPasscodeVC()
        vc.lockBoxId = id
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func lockBoxRegistrationRequest(sender: UIButton) {
        
        guard let userId = UserDefaults.standard.string(forKey: "user_id"),
            let userToken = UserDefaults.standard.string(forKey: "token") else {
                print("No user data in storage")
                return
        }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(userToken)",
            "X-DEVICE-KEY": AppDelegate.currentDeviceUid ?? ""
        ]
        let lockBoxData = [
            "uid": idField.text,
            "name": nameField.text,
            "description": initText
        ]
        
        let apiUrl = LockboxRegistrationVC.API_LOCKBOX_REG_URL.replacingOccurrences(of: "{{user_id}}", with: userId)

        sender.isEnabled = false
        
        let waitingAlert = SpinnerController(style: .processing)
        present(waitingAlert, animated: true) {
            DeviceListVC.manager.request(apiUrl,
                                         method: .post,
                                         parameters: lockBoxData,
                                         encoding: JSONEncoding.default,
                                         headers: headers)
                .responseJSON { [weak self] response in
                    sender.isEnabled = true
                    let requestTitle = "Device registration"
                    
                    waitingAlert.dismiss(animated: false) {
                        guard let strongSelf = self else {
                            print("No self reference is available")
                            return
                        }
                        // if unauthorized
                        if response.response?.statusCode == 401 {
                            strongSelf.goToSplashScreen()
                            return
                        }
                        
                        if response.response?.statusCode == 409 {
                            strongSelf.showPopUp(
                                title: requestTitle,
                                message: "Currently the Parcel Vault is offline. Connect it to the Internet to complete the registration.",
                                style: .error)
                            return
                        }
                        
                        switch response.result {
                        case .success:
                            guard let d = response.data, let regResponseObj = try? JSON(data: d) else {
                                strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                                return
                            }
                            if response.response?.statusCode == 422 {
                                if let msg = regResponseObj["message"].string {
                                    strongSelf.showPopUp(title: requestTitle, message: msg)
                                    print("Request finished with 422, message: \(msg)")
                                    return
                                }
                            }
                            if let status = regResponseObj["status"].string, let msg = regResponseObj["message"].string {
                                strongSelf.showPopUp(title: "Error", message: msg)
                                print("Request finished with status: \(status) and message: \(msg)")
                                return
                            }
                            if regResponseObj["success"].boolValue {
                                strongSelf.goToPasscodeRegistration(with: regResponseObj["lockbox"]["_id"].stringValue)
                            } else {
                                if let errMessage = regResponseObj["error"].string {
                                    strongSelf.showPopUp(title: requestTitle, message: errMessage)
                                } else {
                                    strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                                }
                            }
                        case .failure(let error):
                            if error._code == NSURLErrorTimedOut {
                                strongSelf.showPopUp(
                                    title: requestTitle,
                                    message: "Currently the Parcel Vault is offline. Connect it to the Internet to complete the registration.",
                                    style: .error)
                            } else {
                                strongSelf.showPopUp(title: requestTitle, message: "Network error")
                            }
                        }
                    }
            }
        }
    }
}

// MARK: - Validated Field Delegate

extension LockboxRegistrationVC: ValidatedTextFieldDelegate {
    
    func validatedStateFor(textField: ValidatedTextField) -> Validator.ValidationState {
        
        if textField == nameField {
            return textField.text.isEmpty ? .invalid(reason: "The \"Name\" field cannot be empty.") : .valid
        }
        
        if textField == idField {
            return Validator.shared.validate(parcelVaultId: idField.text)
        }
        
        return .valid
    }
}

// MARK: - InputField Delegate

extension LockboxRegistrationVC: InputFieldVCDelegate {
    
    func inputField(vc: InputFieldVC, didEnter text: String) {
        isDescriptionChanged = true
        if text == "" {
            descValue.text = "Value..."
        } else {
            descValue.text = text
        }
        initText = text
    }
}
