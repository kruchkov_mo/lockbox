//
//  Lockbox.swift
//  LockBox
//
//  Created by max kryuchkov on 17.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import Foundation

class Lockbox {
    
    enum LockboxState: Equatable {
        case locked
        case unlocked
        case offline
        case willBeAvailableAfter(timestamp: TimeInterval)
        
        static var random: LockboxState {
            switch Int.random(min: 0, max: 5) {
            case 0,1: return .locked
            case 2,3: return .unlocked
            case 4: return .offline
            default: return .willBeAvailableAfter(timestamp: TimeInterval(Int.random(min: 60, max: 60 * 60)))
            }
        }
    }
    
    var _id: String?    // Identifier from server
    var id: Int?
    var name: String?
    var descriptionText: String?
    var passcode: String?
    var autoLock: Bool? = true
    
    var state: LockboxState = .unlocked

}
