//
//  AppDelegate.swift
//  LockBox
//
//  Created by max kryuchkov on 14.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import UserNotifications
import OneSignal
import SwiftKeychainWrapper

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    static var shared: AppDelegate!
    
    static var deviceToken: String?
    static var currentDeviceUid: String?
    static var isNotificationsAllowed: Bool = false {
        didSet {
            notificationPermissionChangesClojure?(isNotificationsAllowed)
        }
    }
    static var notificationPermissionChangesClojure: ((Bool)->())?
    static var appDidBecomeActiveClojure: (()->())?
    
    static let lockedStates = [
        "DOOR_LOCKED_BY_APP",
        "DOOR_LOCKED_BY_TIMEOUT",
        "DOOR_UNLOCKED_BY_APP",
        "DOOR_UNLOCKED_BY_PIN",
        "DOOR_TAMPERED",
        "DOOR_CLOSED",
        "ONLINE",
        "OFFLINE"
    ]
 
    let updateDeviceListEvents = [
        "KEYPAD_LOCKED",
        "KEYPAD_UNLOCKED",
        "PIN_REJECTED",
        "DOOR_UNLOCKED_BY_PIN",
        "DOOR_UNLOCKED_BY_APP",
        "DOOR_LOCKED_BY_APP",
        "DOOR_LOCKED_BY_TIMEOUT",
        "DOOR_OPENED",
        "DOOR_CLOSED",
        "DOOR_TAMPERED",
        "ONLINE",
        "OFFLINE"
    ]
    
    var window: UIWindow?
    
    private func setUpDeviceUid() {
        if let devUid = KeychainWrapper.standard.string(forKey: "current_device_unique_id") {
            AppDelegate.currentDeviceUid = devUid
        } else {
            guard let vendorId = UIDevice.current.identifierForVendor?.uuidString else {
                print("Failed to get vendor ID")
                return
            }
            if !KeychainWrapper.standard.set(vendorId, forKey: "current_device_unique_id") {
                print("Failed to add device ID to keychain")
                return
            }
            AppDelegate.currentDeviceUid = vendorId
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        AppDelegate.shared = self
        
        setKeyboardManager()
        
        setUpDeviceUid()
        
        let vc = SplashVC()
        
        let navigationVC = UINavigationController(rootViewController: vc)
        navigationVC.setNavigationBarHidden(true, animated: false)
        
        window?.rootViewController = navigationVC
        window?.makeKeyAndVisible()
        
        // Register push notifications
        registerForPushNotifications()
        
        
        // One Signal push notification
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "79f492bb-ed73-4ad7-812e-ef4fc82f5af5",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
            AppDelegate.isNotificationsAllowed = accepted
        })
        
        if let notification = launchOptions?[.remoteNotification] as? [String: AnyObject],
            let custom = notification["custom"] as? [String: AnyObject] {
            let a = custom["a"] as? [String: AnyObject]
            if let data = a?["data"], let event = data["event"] as? String, let deviceId = data["id"] as? String {
                pushReaction(for: event, id: deviceId)
                print("didFinishLaunchingWithOptions() Push data\nEvent: \(event); UID: \(deviceId)")
            }
        } else {
            print("No push data")
        }
        
        return true
    }
    
    private func goToDeviceList() {
        if UserDefaults.standard.object(forKey: "user_id") != nil {
            let vc = DeviceListVC()
            let navigationVC = UINavigationController(rootViewController: vc)
            navigationVC.setNavigationBarHidden(true, animated: false)
            self.window?.set(toRootViewController: navigationVC)
        }
    }
    
    private func pushReaction(for event: String, id: String) {
        guard let navVC = window?.rootViewController as? UINavigationController else {
            return
        }
      
        switch navVC.topViewController {
            case let deviceListVC as DeviceListVC:
                if updateDeviceListEvents.contains(event) {
                    deviceListVC.updateItem(by: id, event)
                }
            case let detailsVC as LockboxDetailsVC:
                if event == "OFFLINE" {
                    detailsVC.makeFields(active: false)
                } else if event == "ONLINE" {
                    detailsVC.makeFields(active: true)
                }
            case let passwordChangingVC as PasswordChangingLockboxVC:
                if event == "OFFLINE" {
                    let alert = AlertController(
                        title: "Password changing",
                        message: "The Parcel Vault has just gone offline.",
                        style: .default)
                    let okAction = AlertAction(title: "Ok", action: {
                        navVC.popViewController(animated: true)
                    })
                    alert.add(action: okAction)
                    passwordChangingVC.present(alert, animated: true, completion: nil)
                }
            case is EventHistoryVC: if event == "OFFLINE" { return }
            case is InputFieldVC: if event == "OFFLINE" { return }
            
            default:
                if AppDelegate.lockedStates.contains(event) {
                    goToDeviceList()
                }
        }
    }
    
    // MARK: - Setup Libraries
    
    private func setKeyboardManager() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.toolbarPreviousNextAllowedClasses = [UIStackView.self]
    }
    
    // MARK: - Push notifications
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        AppDelegate.deviceToken = tokenParts.joined()
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register push notifications: \(error)")
    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Push notifications permission granted: \(granted)")
            AppDelegate.isNotificationsAllowed = granted
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            AppDelegate.isNotificationsAllowed = granted
            AppDelegate.appDidBecomeActiveClojure?()
        }
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

        if let custom = userInfo["custom"] as? [String: AnyObject] {
            let a = custom["a"] as? [String: AnyObject]
            if let data = a?["data"], let event = data["event"] as? String, let deviceId = data["id"] as? String {
                pushReaction(for: event, id: deviceId)
                
                print("didReceiveRemoteNotification() Push data\nEvent: \(event); UID: \(deviceId)")
            }
        }
    }
}
