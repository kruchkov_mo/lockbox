//
//  UIColor+Brands.swift
//  LockBox
//
//  Created by max kryuchkov on 14.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import ConvenientKit

extension UIColor {
    
    static let defaultYellow = UIColor(colorWithHexValue: 0xFAB800)
}
