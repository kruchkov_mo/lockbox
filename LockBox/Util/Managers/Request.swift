//
//  Request.swift
//  LockBox
//
//  Created by max kruchkov on 10/4/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Request {
    
    static func go(vc: BasicVC,
                   api: String,
                   method: HTTPMethod,
                   headers: HTTPHeaders?,
                   paramData: Parameters?,
                   requestTitle: String,
                   onSuccessClojure: @escaping (JSON)->()) {
        if !Connectivity.isConnectedToInternet() {
            vc.showPopUp(title: requestTitle, message: "The Internet connection appears to be offline")
            return
        }
        Alamofire.request(api,
                          method: method,
                          parameters: paramData,
                          encoding: JSONEncoding.default,
                          headers: headers)
            .responseJSON { [weak vc] response in
                
                guard let strongSelf = vc else {
                    print("No self reference is available")
                    return
                }
                // if unauthorized
                if response.response?.statusCode == 401 {
                    strongSelf.goToSplashScreen()
                    return
                }
                switch response.result {
                case .success:
                    guard let d = response.data, let regResponseObj = try? JSON(data: d) else {
                        strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                        return
                    }
                    if let status = regResponseObj["status"].string, let msg = regResponseObj["message"].string {
                        strongSelf.showPopUp(title: "Error", message: msg)
                        print("Request finished with status: \(status) and message: \(msg)")
                        return
                    }
                    if regResponseObj["success"].boolValue {
                        onSuccessClojure(regResponseObj)
                    } else {
                        if let errMessage = regResponseObj["error"].string {
                            strongSelf.showPopUp(title: requestTitle, message: errMessage)
                        } else {
                            strongSelf.showPopUp(title: requestTitle, message: "Unknown error")
                        }
                    }
                case .failure(_):
                    strongSelf.showPopUp(title: requestTitle, message: "Network error")
                }
        }
    }
}
