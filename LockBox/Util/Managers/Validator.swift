//
//  Validator.swift
//  LockBox
//
//  Created by max kryuchkov on 14.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import Foundation

class Validator {
    
    private let defaultMessage = "Something went wrong..."
    private let onlyDigitsMessage = "Only digits can be entered here."
    private let pinShouldBeNumberDigitsMessage = "The passcode should contain only {number} digits."
    private let pinShouldBeSymbolsAndDigitsMessage = "The Device ID should contain only A-F symbols and digits."
    private let pinShouldBeNumberSymbolsMessage = "The Device ID should be exactly {number} symbols."
    
    enum ValidationState: CustomStringConvertible, Equatable {
        case valid
        case invalid(reason: String)
        
        var description: String {
            switch self {
            case .valid: return "Valid"
            case .invalid(let reason): return reason
            }
        }
        
        var errorMessage: String? {
            switch self {
            case .valid: return nil
            case .invalid(let reason): return reason
            }
        }
    }
    
    static let shared = Validator()
    private init() {}
    
    func validate(email: String?) -> ValidationState {
        if email == nil || email!.isEmpty {
            return .invalid(reason: defaultMessage)
        }
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let isValid = emailTest.evaluate(with: email!)
        
        if !isValid {
            return .invalid(reason: defaultMessage)
        }
        
        return .valid
    }
    
    func validate(password: String?) -> ValidationState {
        if password == nil || password!.isEmpty {
            return .invalid(reason: defaultMessage)
        }
        
        if password!.count < 4 || password!.count > 16 {
            return .invalid(reason: defaultMessage)
        }
        
        return .valid
    }
    
    func validate(pinCode: String?) -> ValidationState {
        guard let pin = pinCode else {
            return .invalid(reason: defaultMessage)
        }
        
        if pin.isEmpty {
            return .invalid(reason: defaultMessage)
        }
        
        if !(pin.count == 4) {
            return .invalid(reason: pinShouldBeNumberDigitsMessage.replacingOccurrences(of: "{number}", with: "4"))
        }
        if !pin.isContainsDigitsOnly {
            return .invalid(reason: onlyDigitsMessage)
        }
        return .valid
    }
    
    func validate(parcelVaultId: String?) -> ValidationState {
        guard let vaultId = parcelVaultId else {
            return .invalid(reason: defaultMessage)
        }
        
        if vaultId.isEmpty {
            return .invalid(reason: defaultMessage)
        }
        
        if vaultId.count != 12 {
            return .invalid(reason: pinShouldBeNumberSymbolsMessage.replacingOccurrences(of: "{number}", with: "12"))
        }
        
        if !vaultId.isAlphanumeric {
            return .invalid(reason: pinShouldBeSymbolsAndDigitsMessage)
        }
        
        return .valid
    }
}

extension String {
    var isAlphanumeric: Bool {
        return range(of: "[^a-fA-F0-9]", options: .regularExpression) == nil
    }
    
    var isContainsDigitsOnly: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
}
