//
//  ResourceUtil.swift
//  LockBox
//
//  Created by max kruchkov on 10/03/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import Foundation

class ResourceUtil {
    private init() {}
    
    static let FILE_NAME = "api_config"
    static let SERVER_ADDRESS = {
        return getApiString(key: "server_address")
    }()
    
    static let REQUEST_TIMEOUT: TimeInterval = {
        return TimeInterval(getApiInt(key: "request_timeout_sec"))
    }()
    
    static func getApiString(key: String) -> String {
        if let path = Bundle.main.path(forResource: ResourceUtil.FILE_NAME, ofType: "plist"),
            let myDict = NSDictionary(contentsOfFile: path) {
            if let entityApi = myDict.value(forKey: key) as? String {
                return entityApi
            }
        }
        
        return ""
    }
    
    static func getApiInt(key: String) -> Int {
        if let path = Bundle.main.path(forResource: ResourceUtil.FILE_NAME, ofType: "plist"),
            let myDict = NSDictionary(contentsOfFile: path) {
            if let entityApi = myDict.value(forKey: key) as? Int {
                return entityApi
            }
        }
        
        return 0
    }
    
    static func getApiUrl(key: String) -> String {
        return SERVER_ADDRESS + getApiString(key: key)
    }
}
