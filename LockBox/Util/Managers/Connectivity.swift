//
//  Connectivity.swift
//  LockBox
//
//  Created by max kruchkov on 10/2/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
