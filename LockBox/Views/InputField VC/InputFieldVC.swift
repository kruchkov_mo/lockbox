//
//  InputFieldVC.swift
//  LockBox
//
//  Created by max kryuchkov on 17.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

protocol InputFieldVCDelegate: class {
    func inputField(vc: InputFieldVC, didEnter text: String)
}

class InputFieldVC: BasicVC {
    
    enum FieldType {
        case singleLine
        case multiLine
    }
    
    // MARK: - Properties

    weak var delegate: InputFieldVCDelegate!
    
    let type: FieldType
    let titleText: String
    
    var enteredText: String {
        switch type {
        case .singleLine: return textField.text ?? ""
        case .multiLine: return textView.text ?? ""
        }
    }
    
    var initialText: String = ""
    
    // MARK: - Outlets
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var singleLineContainer: UIView!
    @IBOutlet weak var multiLineContainer: UIView!
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textView: UITextView!

    // MARK: - Lifecycle

    init(titleText: String, initialText: String = "", type: FieldType, delegate: InputFieldVCDelegate) {
        self.type = type
        self.delegate = delegate
        self.titleText = titleText
        self.initialText = initialText
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if type == .singleLine {
            singleLineContainer.isHidden = false
            multiLineContainer.isHidden = true
        } else {
            singleLineContainer.isHidden = true
            multiLineContainer.isHidden = false
        }
        
        titleLabel.text = titleText
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if type == .singleLine {
            textField.becomeFirstResponder()
        } else {
            textView.becomeFirstResponder()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        IQKeyboardManager.shared.enable = false
        
        if type == .singleLine {
            textField.text = initialText
        } else {
            textView.text = initialText
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        IQKeyboardManager.shared.enable = true
    }

    // MARK: - Actions
    
    @IBAction func backTapped(_ sender: Any) {
        let currentText: String
        if self.type == .singleLine {
            currentText = textField.text ?? ""
        } else {
            currentText = textView.text ?? ""
        }
        
        if initialText == currentText {
            self.navigationController?.popViewController(animated: true)
            return
        }
        dismissKeyboard()
        
        let alert = AlertController(
            title: "Do you want to save changes?",
            message: "",
            style: .default)
        
        let saveAction = AlertAction(title: "Save", action: {
            let textLength = self.type == .singleLine ? (self.textField.text ?? "").count : (self.textView.text ?? "").count
            if self.type == .singleLine && textLength < 4 {
                self.showPopUp(title: "Device name", message: "\"name\" length must be at least 4 characters long", style: .error)
            } else if self.type == .multiLine && textLength < 4 && textLength != 0 {
                self.showPopUp(title: "Device description", message: "\"description\" length must be at least 4 characters long", style: .error)
            } else {
                self.completeAndSave()
            }
        })
        
        let discardAction = AlertAction(title: "Discard", action: {
            self.navigationController?.popViewController(animated: true)
        })
        
        let cancelAction = AlertAction(title: "Cancel", action: {
            if self.type == .singleLine {
                self.textField.becomeFirstResponder()
            } else {
                self.textView.becomeFirstResponder()
            }
        })
        
        alert.add(actions: [saveAction, discardAction, cancelAction])
        present(alert, animated: true, completion: nil)
    }
    
    func completeAndSave() {
        self.delegate.inputField(vc: self, didEnter: self.enteredText)
        self.navigationController?.popViewController(animated: true)
    }

}

extension InputFieldVC: UITextViewDelegate, UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        backTapped(textView)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        backTapped(textField)
    }
    
}
