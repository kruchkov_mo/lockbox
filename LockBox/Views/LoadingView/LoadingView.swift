//
//  LoadingView.swift
//  FreeStart
//
//  Created by max kryuchkov on 31.08.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import ConvenientKit

class LoadingView: ViewFromNib {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
}
