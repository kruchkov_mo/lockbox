//
//  SpinnerController.swift
//  LockBox
//
//  Created by max kruchkov on 14.09.2018.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import UIKit
import ConvenientKit

class SpinnerController: BasicVC {
    
    enum Style {
        case `default`
        case processing
        case error
    }
    
    // MARK: - Outlets

    @IBOutlet weak var alertIndicator: UIActivityIndicatorView!

    // MARK: - Properties

    private var style: Style = .default
    
    // MARK: - Lifecycle

    init(style: Style) {
        super.init()
        
        self.style = style
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alertIndicator.isHidden = false
        alertIndicator.startAnimating()
    }
    
    // MARK: - Setup

    private func setup() {
        modalPresentationStyle = .overCurrentContext
        providesPresentationContextTransitionStyle = true
        modalTransitionStyle = .crossDissolve
        definesPresentationContext = true
    }
    
}
