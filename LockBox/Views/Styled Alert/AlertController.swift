//
//  AlertController.swift
//  LockBox
//
//  Created by max kryuchkov on 14.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import ConvenientKit

struct AlertAction {
    let title: String
    let action: () -> ()
}

class AlertController: BasicVC {
    
    enum Style {
        case `default`
        case processing
        case error
    }
    
    // MARK: - Outlets

    @IBOutlet weak var alertIndicator: UIActivityIndicatorView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var errorIcon: UIImageView!
    @IBOutlet weak var stackView: UIStackView!

    // MARK: - Properties

    private var style: Style = .default
    private var titleText: String = ""
    private var message: String = ""
    private var actions = [AlertAction]()
    
    // MARK: - Lifecycle

    init(title: String, message: String, style: Style) {
        super.init()
        
        self.style = style
        self.titleText = title
        self.message = message
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alertIndicator.isHidden = true
        
        titleLabel.text = titleText
        textLabel.text = message
        
        if style == .error {
            titleLabel.textColor = UIColor.init(red: 255.0/255.0, green: 88.0/255.0, blue: 78.0/255.0, alpha: 1.0)
            errorIcon.isHidden = false
        } else if style == .processing {
            alertIndicator.isHidden = false
            alertIndicator.startAnimating()
        } else {
            titleLabel.textColor = UIColor.black
            errorIcon.isHidden = true
        }
        
        displayAction()
    }
    
    // MARK: - Setup

    private func setup() {
        modalPresentationStyle = .overCurrentContext
        providesPresentationContextTransitionStyle = true
        modalTransitionStyle = .crossDissolve
        definesPresentationContext = true
    }
    
    private func displayAction() {
        actions.forEach({ action in
            
            let label = makeStyledLabel(text: action.title)
            
            UITapGestureRecognizer(addToView: label, closure: {
                self.dismiss(animated: true, completion: {
                    action.action()
                })
            })
            
            self.stackView.addArrangedSubview(label)
        })
    }
    
    // MARK: - Actions

    func add(action: AlertAction) {
        actions.append(action)
    }
    
    func add(actions: [AlertAction]) {
        actions.forEach({ self.add(action: $0) })
    }
    
    // MARK: - Helpers

    private func makeStyledLabel(text: String) -> UILabel {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17.0, weight: .semibold)
        label.textColor = UIColor(colorWithHexValue: 0xF8F8F8)
        label.textAlignment = .center
        label.lineBreakMode = .byTruncatingTail
        label.isUserInteractionEnabled = true
        label.text = text
        
        return label
    }
    
}
