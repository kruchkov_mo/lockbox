//
//  ValidatedTextField.swift
//  LockBox
//
//  Created by max kryuchkov on 14.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import ConvenientKit
import IQKeyboardManagerSwift

protocol ValidatedTextFieldDelegate: class {
    func validatedStateFor(textField: ValidatedTextField) -> Validator.ValidationState
}

class ValidatedTextField: ViewFromNib {
    
    static let height: CGFloat = 50.0
    
    enum State: Equatable {
        case blank
        case focused
        case filled
        case error(message: String)
    }
    
    enum SecureState: Equatable {
        case notSecure
        case secure(isTextVisible: Bool)
    }

    // MARK: - Properties
    
    weak var delegate: ValidatedTextFieldDelegate?

    var state: State = .blank {
        didSet {
            updateUI()
        }
    }
    
    var secureState: SecureState = .notSecure {
        didSet {
            updateUI()
        }
    }
    
    var text: String {
        get {
            return textField?.text ?? ""
        } set {
            textField?.text = newValue
        }
    }
    
    var placeholder: String {
        get {
            return textField?.placeholder ?? ""
        } set {
            textField?.placeholder = newValue
        }
    }
    
    // MARK: - Outlets

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var underline: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var secureIcon: UIButton!

    // MARK: - UI
    
    func updateUI() {
        switch secureState {
        case .notSecure:
            textField.isSecureTextEntry = false
            secureIcon.isHidden = true
        case .secure(let isTextVisible):
            textField.isSecureTextEntry = !isTextVisible
            secureIcon.isHidden = false
            secureIcon.setImage(isTextVisible ? #imageLiteral(resourceName: "eye-closed") : #imageLiteral(resourceName: "eye-normal"), for: .normal)
            
            if #available(iOS 12.0, *) {
            } else {
                if let t = textField.text, t.count > 0 {
                    textField.text = "?"
                    textField.text = t
                    let newPosition = textField.endOfDocument
                    textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                }
            }
        }
        
        switch state {
        case .blank:
            underline.backgroundColor = UIColor(colorWithHexValue: 0xD9D9D9)
            errorLabel.text = ""
        case .focused:
            underline.backgroundColor = UIColor.defaultYellow
            errorLabel.text = ""
        case .filled:
            underline.backgroundColor = UIColor(colorWithHexValue: 0xD9D9D9)
            errorLabel.text = ""
        case .error(let message):
            errorLabel.text = message
            underline.backgroundColor = UIColor(colorWithHexValue: 0xFF584E)
        }
    }
    
    override func setupViews() {
        textField.keyboardDistanceFromTextField = 32.0
    }

    // MARK: - Actions
    
    @IBAction func showHideTapped(_ sender: Any) {
        switch secureState {
        case .notSecure: return
        case .secure(let isTextVisible):
            secureState = .secure(isTextVisible: !isTextVisible)
        }
    }

}

extension ValidatedTextField: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        state = .focused
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        if (textField.text ?? "").isEmpty {
            return state = .blank
        }
        
        let validationState = delegate?.validatedStateFor(textField: self) ?? .valid
        switch validationState {
        case .valid:
            state = .filled
        case .invalid(let reason):
            state = .error(message: reason)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
}
