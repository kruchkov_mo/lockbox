//
//  CustomSwitchThumbView.swift
//  LockBox
//
//  Created by max kryuchkov on 15.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit

class CustomSwitchThumbView: UIView {
    
    private(set) var thumbImageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(thumbImageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addSubview(thumbImageView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        thumbImageView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        thumbImageView.cornerRadius = cornerRadius
        thumbImageView.clipsToBounds = clipsToBounds
    }
}
