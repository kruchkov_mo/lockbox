//
//  CustomSwitch.swift
//  LockBox
//
//  Created by max kryuchkov on 15.09.2018.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit

/**
 source: https://github.com/factoryhr/CustomUISwitch
 */
@IBDesignable
class CustomSwitch: UIControl {
    
    // MARK: Public properties
    
    @IBInspectable public var isOn = true
    
    public var animationDuration = 0.5
    
    @IBInspectable  public var padding: CGFloat = 1 {
        didSet {
            layoutSubviews()
        }
    }
    
    @IBInspectable  public var onTintColor = UIColor(red: 144/255, green: 202/255, blue: 119/255, alpha: 1) {
        didSet {
            setupUI()
        }
    }
    
    @IBInspectable public var offTintColor = UIColor.black {
        didSet {
            setupUI()
        }
    }
    
    @IBInspectable open var cornerRadiusS: CGFloat {
        get {
            return privateCornerRadius
        } set {
            if newValue > 0.5 || newValue < 0.0 {
                privateCornerRadius = 0.5
            } else {
                privateCornerRadius = newValue
            }
        }
    }
    
    private var privateCornerRadius: CGFloat = 0.5 {
        didSet {
            layoutSubviews()
            
        }
    }
    
    // thumb properties
    @IBInspectable public var thumbTintColor = UIColor.white {
        didSet {
            thumbView.backgroundColor = thumbTintColor
        }
    }
    
    @IBInspectable public var thumbCornerRadius: CGFloat {
        get {
            return privateThumbCornerRadius
        } set {
            if newValue > 0.5 || newValue < 0.0 {
                privateThumbCornerRadius = 0.5
            } else {
                privateThumbCornerRadius = newValue
            }
        }
        
    }
    
    private var privateThumbCornerRadius: CGFloat = 0.5 {
        didSet {
            layoutSubviews()
            
        }
    }
    
    @IBInspectable public var thumbSize = CGSize.zero {
        didSet {
            layoutSubviews()
        }
    }
    
    @IBInspectable public var thumbImage: UIImage? = nil {
        didSet {
            guard let image = thumbImage else { return }
            thumbView.thumbImageView.image = image
        }
    }
    
    public var onImage: UIImage? {
        didSet {
            onImageView.image = onImage
            layoutSubviews()
        }
    }
    
    public var offImage: UIImage? {
        didSet {
            offImageView.image = offImage
            layoutSubviews()
        }
    }
    
    @IBInspectable public var thumbShadowColor = UIColor.black {
        didSet {
            thumbView.shadowColor = thumbShadowColor
        }
    }
    
    @IBInspectable public var thumbShadowOffset = CGSize(width: 0.75, height: 2) {
        didSet {
            thumbView.shadowOffset = thumbShadowOffset
        }
    }
    
    @IBInspectable public var thumbShadowRadius: CGFloat = 1.5 {
        didSet {
            thumbView.shadowRadius = thumbShadowRadius
        }
    }
    
    @IBInspectable public var thumbShadowOpacity: CGFloat = 0.4 {
        didSet {
            thumbView.shadowOpacity = thumbShadowOpacity
        }
    }
    
    // labels
    
    public var labelOff = UILabel()
    public var labelOn = UILabel()
    
    public var areLabelsShown = false {
        didSet {
            setupUI()
        }
    }
    
    // MARK: - Private properties
    
    private var thumbView = CustomSwitchThumbView(frame: CGRect.zero)
    private var onImageView = UIImageView(frame: CGRect.zero)
    private var offImageView = UIImageView(frame: CGRect.zero)
    
    private var onPoint = CGPoint.zero
    private var offPoint = CGPoint.zero
    private var isAnimating = false
    
    // MARK: - Lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
}

// MARK: - Private methods
extension CustomSwitch {
    
    private func setupUI() {
        
        // clear self before configuration
        clear()
        
        clipsToBounds = false
        
        // configure thumb view
        thumbView.backgroundColor = thumbTintColor
        thumbView.isUserInteractionEnabled = false
        
        // dodati kasnije
        thumbView.shadowColor = thumbShadowColor
        thumbView.shadowRadius = thumbShadowRadius
        thumbView.shadowOpacity = thumbShadowOpacity
        thumbView.shadowOffset = thumbShadowOffset
        
        backgroundColor = isOn ? onTintColor : offTintColor
        
        addSubview(thumbView)
        addSubview(onImageView)
        addSubview(offImageView)
        
        setupLabels()
    }
    
    private func clear() {
        for view in subviews {
            view.removeFromSuperview()
        }
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.beginTracking(touch, with: event)
        
        animate()
        return true
    }
    
    func setOn(on:Bool, animated:Bool) {
        
        switch animated {
        case true:
            animate(on: on)
        case false:
            isOn = on
            setupViewsOnAction()
            completeAction()
        }
        
    }
    
    private func animate(on: Bool? = nil) {
        
        isOn = on ?? !isOn
        
        isAnimating = true
        
        UIView.animate(withDuration: animationDuration, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.beginFromCurrentState, UIViewAnimationOptions.allowUserInteraction], animations: {
            self.setupViewsOnAction()
            
        }, completion: { _ in
            self.completeAction()
        })
    }
    
    private func setupViewsOnAction() {
        thumbView.frame.origin.x = isOn ? onPoint.x : offPoint.x
        backgroundColor = isOn ? onTintColor : offTintColor
        
        labelOn.alpha = isOn ? 1.0 : 0.0
        labelOff.alpha = isOn ? 0.0 : 1.0
        
        setOnOffImageFrame()
    }
    
    private func completeAction() {
        isAnimating = false
        sendActions(for: UIControlEvents.valueChanged)
    }
}

// MARK: - Public methods
extension CustomSwitch {
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        if !isAnimating {
            layer.cornerRadius = bounds.size.height * cornerRadiusS
            backgroundColor = isOn ? onTintColor : offTintColor
            
            // thumb managment
            // get thumb size, if none set, use one from bounds
            let thumbSize = self.thumbSize != CGSize.zero ? self.thumbSize : CGSize(width: bounds.size.height - 2, height: bounds.height - 2)
            let yPostition = (bounds.size.height - thumbSize.height) / 2
            
            onPoint = CGPoint(x: bounds.size.width - thumbSize.width - padding, y: yPostition)
            offPoint = CGPoint(x: padding, y: yPostition)
            
            thumbView.frame = CGRect(origin: isOn ? onPoint : offPoint, size: thumbSize)
            thumbView.cornerRadius = thumbSize.height * thumbCornerRadius
            
            
            // label frame
            if areLabelsShown {
                
                let yPoint = (frame.height - labelOn.frame.height) / 2.0
                let xPointOffset: CGFloat = 8.0
                labelOn.frame.origin = CGPoint(x: xPointOffset, y: yPoint)
                labelOff.frame.origin = CGPoint(x: frame.width - labelOff.frame.width - xPointOffset, y: yPoint)
            }
            
            // on/off images
            // set to preserve aspect ratio of image in thumbView
            
            guard onImage != nil && offImage != nil else { return }
            
            let frameSize = thumbSize.width > thumbSize.height ? thumbSize.height * 0.7 : thumbSize.width * 0.7
            
            let onOffImageSize = CGSize(width: frameSize, height: frameSize)
            
            
            onImageView.frame.size = onOffImageSize
            offImageView.frame.size = onOffImageSize
            
            onImageView.center = CGPoint(x: onPoint.x + thumbView.frame.size.width / 2, y: thumbView.center.y)
            offImageView.center = CGPoint(x: offPoint.x + thumbView.frame.size.width / 2, y: thumbView.center.y)
            
            
            onImageView.alpha = isOn ? 1.0 : 0.0
            offImageView.alpha = isOn ? 0.0 : 1.0
            
        }
    }
}

//MARK: - Labels frame
extension CustomSwitch {
    
    func setupLabels() {
        guard areLabelsShown else {
            labelOff.alpha = 0
            labelOn.alpha = 0
            return
        }
        
        labelOn.alpha = isOn ? 1.0 : 0.0
        labelOff.alpha = isOn ? 0.0 : 1.0
        
        let yPoint = (frame.height - labelOn.frame.height) / 2.0
        let xPointOffset: CGFloat = 8.0
        labelOn.frame.origin = CGPoint(x: xPointOffset, y: yPoint)
        labelOff.frame.origin = CGPoint(x: frame.width - labelOff.frame.width - xPointOffset, y: yPoint)
        
        insertSubview(labelOff, belowSubview: thumbView)
        insertSubview(labelOn, belowSubview: thumbView)
        
    }
    
}

// MARK: - Animating on/off images
extension CustomSwitch {
    
    private func setOnOffImageFrame() {
        guard onImage != nil && offImage != nil else { return }
        
        onImageView.center.x = isOn ? onPoint.x + thumbView.frame.size.width / 2 : frame.width
        offImageView.center.x = !isOn ? offPoint.x + thumbView.frame.size.width / 2 : 0
        onImageView.alpha = isOn ? 1.0 : 0.0
        offImageView.alpha = isOn ? 0.0 : 1.0
        
    }
}
